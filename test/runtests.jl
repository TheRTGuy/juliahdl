using JuliaHDL
using Test

this_file = @__FILE__
files = readdir(dirname(this_file))
for file in files
    if file != basename(this_file) && !isdir(file)
        include(file)
    end
end