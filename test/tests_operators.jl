if !all( isdefined.(Ref(Main), [:JuliaHDL, :Test]) )
    using JuliaHDL, Test
end
@testset verbose=true "Test operator type consistency and VHDL code output" begin
    a, b, c = [Usig(5) for _ in 1:3]
    d, e = Ssig(5), Lsig(5)
    i, j = Isig(), Isig{-5:5}()
    name = "Test"
    lang = VHDL_Lang()

    @testset "Arithmetic operators" begin
        ops = [(+, :+), (-, :-), (*, :*), (/, :/)]
        for (op, sym) in ops
            ab = op(a, b)
            ij = op(i, j)
            ji = op(j, i)
            
            @test ab isa Usig{5} && 
            ab.operator == sym && length(ab.from) == 2
            
            @test ij isa Isig && ij.operator == sym && length(ij.from) == 2          
            @test ji isa Isig && ji.operator == sym && length(ji.from) == 2             
            @test range(ij) === range(ji)                            
            @test_throws MethodError op(a, d) #can't +,-,*,/ signed and unsigned
            @test_throws MethodError op(a, e) #can't +,-,*,/ unsigned and logic vector
            @test_throws MethodError op(a, i) #Can't do arithmetic between integer and bits types
            @test_throws MethodError op(i, a) 
            @test_throws MethodError op(e, e) #arithmetic not allowed on logical vectors
            @test JuliaHDL.prim_type_str(ab, lang) == "unsigned(4 downto 0)"
            
            if op !== -
                abc = op(a, b, c)
                @test abc isa Usig{5} && 
                                    abc.operator == sym && length(abc.from) == 3   
                @test JuliaHDL.prim_type_str(abc, lang) == "unsigned(4 downto 0)"
            end

            aconst = op(a, 1)
            consta = op(1, a)
            @test aconst isa Usig{5} && aconst.operator == sym && 
                    length(aconst.from) == 2 && aconst.from[2] isa JuliaHDL.ConstSig{Unsigned, 5}

            @test consta isa Usig{5} && consta.operator == sym && 
                    length(consta.from) == 2 && consta.from[1] isa JuliaHDL.ConstSig{Unsigned, 5}

            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)

            JuliaHDL.put_name!(ab, names_dict, buf, lang)
            if op !== -
                JuliaHDL.put_name!(abc, names_dict, buf, lang)
            end
            JuliaHDL.put_name!(aconst, names_dict, buf, lang)
            JuliaHDL.put_name!(consta, names_dict, buf, lang)

            s = read(seekstart(buf), String)
            if op !== -
                @test issubset((a,b,c,ab,abc,aconst,consta), keys(names_dict)) && length(names_dict) == 7
                @test occursin("$(names_dict[abc]) <= $(names_dict[a]) $sym $(names_dict[b]) $sym $(names_dict[c]);", s)
            else
                @test issubset((a,b,ab,aconst,consta), keys(names_dict)) && length(names_dict) == 5
            end
            @test occursin("$(names_dict[ab]) <= $(names_dict[a]) $sym $(names_dict[b]);", s)
            @test occursin("$(names_dict[aconst]) <= $(names_dict[a]) $sym \"00001\";", s)
            @test occursin("$(names_dict[consta]) <= \"00001\" $sym $(names_dict[a]);", s)
        end
        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(write=true, read=true)

        na = -a 
        @test length(na.from) == 1
        JuliaHDL.print_assignment!(na, name, names_dict, buf, lang)

        s = read(seekstart(buf), String)
        @test occursin(name * " <=  - $(names_dict[a]);", s)
    end

    @testset "Boolean operators" begin
        ops = [(&, :and), (|, :or), (xor, :xor)]
        for (op, sym) in ops
            ab = op(a, b)
            nab = ~ab
            @test nab !== ab

            us = SigVec(length(a) |> Usig for _=1:3)
            uop = op( us... )

            @test ab isa JuliaHDL.BitwiseOperator{Unsigned, 5} && 
                                ab.operator == sym && length(ab.from) == 2

            @test uop isa JuliaHDL.BitwiseOperator{Unsigned, 5} && 
                                uop.operator == sym && length(uop.from) == 3

            aconst = op(a, 1)
            consta = op(1, a)
            @test aconst isa Usig{5} && aconst.operator == sym && 
                    length(aconst.from) == 2 && aconst.from[2] isa JuliaHDL.ConstSig{Unsigned, 5}

            @test consta isa Usig{5} && consta.operator == sym && 
                    length(consta.from) == 2 && consta.from[1] isa JuliaHDL.ConstSig{Unsigned, 5}

            @test_throws MethodError op(a, d) #can't +,-,*,/ signed and unsigned
            @test_throws MethodError op(a, e) #can't +,-,*,/ unsigned and logic vector
            @test_throws MethodError op(a, i) #Can't do arithmetic between integer and bits types
            @test_throws MethodError op(i, a) 
            
            name = "Test"
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)

            JuliaHDL.put_name!(ab, names_dict, buf, lang)
            JuliaHDL.put_name!(uop, names_dict, buf, lang)
            JuliaHDL.put_name!(aconst, names_dict, buf, lang)
            JuliaHDL.put_name!(consta, names_dict, buf, lang)
            JuliaHDL.put_name!(nab, names_dict, buf, lang)

            s = read(seekstart(buf), String)
            @test issubset((a,b,us...,uop, ab,aconst,consta,nab), keys(names_dict)) && length(names_dict) == 10

            @test occursin("$(names_dict[ab]) <= $(names_dict[a]) $sym $(names_dict[b]);", s)
            @test occursin("$(names_dict[uop]) <= "*join( ("$(names_dict[u])" for u in us), " $sym ")*";", s)
            @test occursin("$(names_dict[aconst]) <= $(names_dict[a]) $sym \"00001\";", s)
            @test occursin("$(names_dict[consta]) <= \"00001\" $sym $(names_dict[a]);", s)
            @test occursin("$(names_dict[nab]) <= not($(names_dict[a]) $sym $(names_dict[b]));", s)
        end
    end

    @testset "Comparators" begin
        @test (a == b) isa Bool #stupid test, but must be done (we can't overload this operator)
        @test (a != b) isa Bool
        for (op, sym) in [(eq, :(=)), (neq, :/=),
            (<=, :<=), (<, :<),
            (>=, :>=), (>, :>)]

            cmp = op(a, b)
            cmp2 = op(a, 5)
            cmp3 = op(5, a)
            cmp4 = op(a, i)
            cmp5 = op(i, j)
            cmp6 = op(i, 5)

            @test cmp.operator == sym && cmp isa JuliaHDL.CmpOperator
            @test cmp2.operator == sym && cmp2.from[1] == a && cmp2.from[2] == 5uc{5}
            @test cmp3.operator == sym && cmp3.from[1] == 5uc{5} && cmp3.from[2] == a
            @test cmp4.operator == sym && cmp4.from[1] == a && cmp4.from[2] == i
            @test cmp5.operator == sym && cmp5.from[1] == i && cmp5.from[2] == j
            @test cmp6.operator == sym && cmp6.from[1] == i && cmp6.from[2] == 5ic
            @test JuliaHDL.prim_type_str(cmp, lang) == "boolean"
            @test_throws MethodError op(a, d) #can't cmp signed and unsigned
            @test_throws MethodError op(a, e) #can't cmp unsigned and logic vector
            @test_throws MethodError op(e, i) #can't cmp logic and integer
            if op !== eq && op !== neq
                @test_throws MethodError op(e, e) #comp not allowed on logical vectors
            end
            
            name = "Test"
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(cmp, names_dict, buf, lang)
            JuliaHDL.put_name!(cmp2, names_dict, buf, lang)

            s = read(seekstart(buf), String)
            @test issubset((a, b, cmp, cmp2), keys(names_dict)) && length(names_dict) == 4
            @test occursin("$(names_dict[cmp]) <= $(names_dict[a]) $sym $(names_dict[b]);\n", s)
            @test occursin("$(names_dict[cmp2]) <= $(names_dict[a]) $sym \"00101\";\n", s)
        end
    end

    @testset "Shift operators" begin
        for (op, sym) in [(<<, :shift_left), (>>, :shift_right),
            (rol, :rotate_left), (ror, :rotate_right)]

            ncount = 3

            shift = op(a, ncount)

            @test shift.operator == sym && shift isa JuliaHDL.ShiftOp{Unsigned, 5}
            @test_throws MethodError op(a, d) 
            @test_throws MethodError op(a, e) 
            @test_throws MethodError op(e, e) 

            name = "Test"
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.print_assignment!(shift, name, names_dict, buf, lang)
            s = read(seekstart(buf), String)
            @test occursin(name * " <= $sym($(names_dict[a]), $(ncount));", s)
        end
    end
end

#The following tests are not so useful to show whether reductions work
#on vectors of AbstractSignals - of course they do, as long as we have simple 
#operations like addition and multiplication defined. However, they generate
#nice cases which test other features of the code generator
@testset verbose=true "Test reductions on vectors of AbstractSignals (VHDL)" begin
    xs = SigVec(Usig(5) for _ = 1:10)
    lang = VHDL_Lang()

    @testset "Product and sum" begin
        ops = [(sum, :+), (prod, :*)]
        for (f, op) in ops
            s = f(xs)

            name = "Test"
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)

            JuliaHDL.print_assignment!(s, name, names_dict, buf, lang)

            @test issubset(xs, keys(names_dict))

            n = s
            for p in @view(xs[end:-1:2])
                @test n.from[2] == p && n.operator == op
                n = n.from[1]
                @test n in keys(names_dict)
            end
            @test n == xs[1]
        end
    end

    @testset "Difference" begin
        bs = diff(xs)

        @test length(bs) == length(xs) - 1
        @test all( [b.operator == :- && 
                    b.from[1] == xs[i+1] &&
                    b.from[2] == xs[i]  for (i, b) in enumerate(bs)] )
    end

    @testset "Dot product" begin
        bs = [Usig(5) for _ = 1:10]

        c = xs' * bs

        name = "Test"
        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(write=true, read=true)

        JuliaHDL.print_assignment!(c, name, names_dict, buf, lang)
        @test issubset(xs, keys(names_dict)) 
        @test issubset(bs, keys(names_dict))

        n = c
        for i = length(xs):-1:2
            mulop = n.from[2]
            @test n.operator == :+ && mulop.operator == :* &&
                    mulop.from[1] == xs[i] && mulop.from[2] == bs[i] &&
                    mulop in keys(names_dict)
            n = n.from[1]
            @test n in keys(names_dict)
        end
        @test n.operator == :* && n.from[1] == xs[1] && n.from[2] == bs[1]
    end

    @testset "Matrix mul" begin
        A = SigMat(Usig(5) for _=1:2,_=1:10)
        y = A*xs

        @test length(y) == 2 && y isa UsigVec{5, 2}

        for (c, as) in zip(y, eachrow(A))
            n = c
            for i = length(as):-1:2
                mulop = n.from[2]
                @test n.operator == :+ && mulop.operator == :* &&
                        mulop.from[1] == as[i] && mulop.from[2] == xs[i] #= &&
                        mulop in keys(names_dict) =#
                n = n.from[1]
                # @test n in keys(names_dict)
            end
            @test n.operator == :* && n.from[1] == as[1] && n.from[2] == xs[1]
        end
    end
end