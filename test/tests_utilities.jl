for mod in [:JuliaHDL, :Test]
    if !isdefined(Main, mod)
        @eval using $mod
    end
end

@testset verbose=true "Test select macros" begin
    a, b, c = [Usig(5) for _=1:3]
    @testset "Conditional macro" begin
        expr = @macroexpand @select begin
            @when a > b begin
                m = a + b
                n = a * c
            end
            @when a < c begin
                p = a + c
                m = b * a
            end
        end
        args_expected = [ :(m = Cond(nothing, a > b => a + b, a < c => b * a)),
                         :(p = Cond(m, a > b => nothing, a < c => a + c)),
                          :(n = Cond(m, a > b => a * c, a < c => nothing)),
                          :((m,p,n))]                             
        @test expr.head == :(=) && expr.args[1].head === :tuple && expr.args[2].head === :let
        lhs, rhs = expr.args[1:2]
        @test all(arg.head === :block for arg in rhs.args)
        for arg in rhs.args[2].args
            @test arg in args_expected
        end

        @select begin
            @when a > b begin
                m = a + b
                n = a * c
            end
            @when a < c begin
                p = a + c
                m = b * a
            end
        end

        @test m isa Cond{Unsigned, 5}
        @test n isa Cond{Unsigned, 5}
        @test p isa Cond{Unsigned, 5}
    end
    @testset "Mux macro" begin
        sel = Lsig(2)
        cases = (c1 = "00", c2 = "01")
        expr = @macroexpand @select sel begin
            @when cases.c1 begin
                m = a + b
                n = a * c
            end
            @when cases.c2 begin
                p = a + c
                m = b * a
            end
        end
        args_expected = [:(m = Mux(nothing, sel, cases.c1 => a + b, cases.c2 => b * a)),
                         :(p = Mux(m, sel, cases.c2 => a + c)),
                          :(n = Mux(m, sel, cases.c1 => a * c)),
                          :((m,p,n))]                         
        @test expr.head == :(=) && expr.args[1].head === :tuple && expr.args[2].head === :let
        lhs, rhs = expr.args[1:2]
        @test all(arg.head === :block for arg in rhs.args)
        for arg in rhs.args[2].args
            @test arg in args_expected
        end

        @select sel begin
            @when cases.c1 begin
                m = a + b
                n = a * c
            end
            @when cases.c2 begin
                p = a + c
                m = b * a
            end
        end

        @test m isa Mux{Unsigned, 5}
        @test n isa Mux{Unsigned, 5}
        @test p isa Mux{Unsigned, 5}
    end

    @testset "Nested selects #1" begin
        sel = Lsig(2)
        sel2 = Lsig(2)
        cases = (c1 = "00", c2 = "01")

        @select sel begin
            @when cases.c1 begin
                @select begin
                    @when a > b begin
                        c = a
                        d = b
                    end
                    @others begin
                        c = b
                        d = a
                    end
                end
            end
            @when cases.c2 begin
                @select sel2 begin
                    @when cases.c1 c = a + b
                    @when cases.c2 d = a * b
                end
            end
        end

        @test c isa Mux{Unsigned, 5}
        @test c.from[1][2] isa Cond{Unsigned, 5} && 
                c.from[2][2] isa Mux{Unsigned, 5}
        @test c.from[1][2].from[1][2] === a && c.from[1][2].from[2][2] === b
        @test c.from[2][2].from[1][2] === a+b
        @test length(c.from[2][2].from) === 1

        @test d isa Mux{Unsigned, 5}
        @test d.from[1][2] isa Cond{Unsigned, 5} && 
                d.from[2][2] isa Mux{Unsigned, 5}
        @test d.from[1][2].from[1][2] === b && d.from[1][2].from[2][2] === a
        @test d.from[2][2].from[1][2] === a*b
        @test length(d.from[2][2].from) === 1
    end

    @testset "Nested selects #2" begin
        sel = Lsig(2)
        cases = (c1 = "00", c2 = "01")

        @select begin
            @when a > b begin
                @select sel begin
                    @when cases.c1 begin
                        c = a
                        d = b
                    end
                    @when cases.c2 begin
                        c = b
                        d = a
                    end
                end
            end
            @others begin
                @select begin
                    @when (a + b > a * b) c = a + b
                    @others d = a * b
                end
            end
        end

        @test c isa Cond{Unsigned, 5}
        @test c.from[1][2] isa Mux{Unsigned, 5} && 
                c.from[2][2] isa Cond{Unsigned, 5}
        @test c.from[1][2].from[1][2] === a && c.from[1][2].from[2][2] === b
        @test c.from[2][2].from[1][2] === a+b
        @test c.from[2][2].from[2][2] === nothing
        @test length(c.from[2][2].from) === 2

        @test d isa Cond{Unsigned, 5}
        @test d.from[1][2] isa Mux{Unsigned, 5} && 
                d.from[2][2] isa Cond{Unsigned, 5}
        @test d.from[1][2].from[1][2] === b && d.from[1][2].from[2][2] === a
        @test d.from[2][2].from[1][2] === nothing
        @test d.from[2][2].from[2][2] === a*b
        @test length(d.from[2][2].from) === 2
    end
end

@testset "Dump VHDL" begin
    a, b, c = Usig(5), Usig(5), Usig(5)

    d = (a + b) * c

    buf = IOBuffer(write=true, read=true)
    dump_hdl(d, io=buf)
    s = read(seekstart(buf), String)
    @test contains(s, "============\t Signals\t ============\n")
    @test contains(s, "    signal Unsigned_5 : unsigned(4 downto 0);\n")
    @test contains(s, "    signal Mul_1 : unsigned(4 downto 0);\n")
    @test contains(s, "    signal Unsigned_4 : unsigned(4 downto 0);\n")
    @test contains(s, "    signal Unsigned_3 : unsigned(4 downto 0);\n")
    @test contains(s, "    signal Add_2 : unsigned(4 downto 0);\n")
    @test contains(s, "============\t Assignments\t ============\n")
    @test contains(s, "    Add_2 <= Unsigned_3 + Unsigned_4;\n")
    @test contains(s, "    Mul_1 <= Add_2 * Unsigned_5;\n")
end