if !all( isdefined.(Ref(Main), [:JuliaHDL, :Test]) )
    using JuliaHDL, Test
end

@testset verbose=true "VHDL" begin
    lang = VHDL_Lang()
    @testset verbose=true "Simple types" begin
        n = 5
        for (s, p) in zip(("unsigned", "signed", "std_logic_vector"), (Usig, Ssig, Lsig))
            t = p(n)
            @test JuliaHDL.prim_type_str(t, lang) === s*"($(n-1) downto 0)"
        end
        w, b, i, j, k = Wire(), Boolean(), Isig(), Isig{-2:2}(), Isig{1:5}(1)
        @test JuliaHDL.prim_type_str(w, lang) === "std_logic"
        @test JuliaHDL.prim_type_str(b, lang) === "boolean"
        @test JuliaHDL.prim_type_str(i, lang) === "integer"
        @test JuliaHDL.prim_type_str(j, lang) === "integer range -2 to 2"
        @test JuliaHDL.prim_type_str(k, lang) === "natural range 1 to 5"    
    end

    @testset verbose=true "Name prefix" begin
        for (s, p) in zip(("Unsigned", "Signed", "Logic"), (Usig, Ssig, Lsig))
            t = p(5)
            @test JuliaHDL.get_name(t, 1) === s*"_1"
            @test JuliaHDL.get_name(t, 2) === s*"_2"
        end
        for (s, p) in zip(("\"001\"", "\"001\"", "\"001\"", "0", "1"), 
                          (lc"001", uc"001", sc"001", 0ic, 1ic{1:5}))
            @test JuliaHDL.get_const_val_str(p, lang) === s
        end
        for s in (lc"000", uc"000", sc"000")
            @test JuliaHDL.get_const_val_str(s, lang) === "\"000\""
        end
        @test JuliaHDL.get_const_val_str(bc(true), lang) === "true"
        @test JuliaHDL.get_const_val_str(bc(false), lang) === "false"
        @test JuliaHDL.get_const_val_str(lc"0", lang) === "'0'"
        @test JuliaHDL.get_const_val_str(lc"1", lang) === "'1'"

        a, b = Usig(5), Usig(5)
        for (op, s) in zip((+,-,*,/,
                            &,|,xor,
                            eq,neq,<,<=,>,>=),
                    ("Add", "Sub", "Mul", "Div",
                    "And", "Or", "Xor",
                    "Eq", "Neq", "Lt", "Leq", "Gt", "Geq"))
            c = op(a, b)
            @test JuliaHDL.get_name(c,1) === s * "_1"
            @test JuliaHDL.get_name(c,2) === s * "_2"
        end
        for (op, s) in zip((<<,>>,rol,ror),
                    ("Shl", "Shr", "Rol", "Ror"))
            c = op(a, 2)
            @test JuliaHDL.get_name(c,1) === s * "_1"
            @test JuliaHDL.get_name(c,2) === s * "_2"
        end
        clk = Wire()
        sel = Lsig(2)
        r = Reg(clk, a)
        m = Mux(sel, lc"00" => a + b, lc"01" => a*b, nothing => a+b)
        c = Cond(a > b => a, nothing => b)

        for i = 1:2
            @test JuliaHDL.get_name(clk,i) === "Wire_$i"
            @test JuliaHDL.get_name(r,i) === "Reg_$i"
            @test JuliaHDL.get_name(m,i) === "Mux_$i"
            @test JuliaHDL.get_name(c,i) === "Cond_$i"
            @test JuliaHDL.get_name(a ^ b,i) === "Concat_$i"
            @test JuliaHDL.get_name(a[0=>3],i) === "Slice_$i"
        end
    end

    @testset "Feedback test" begin
        clk, rst = Wire(), Wire()
        dir = Lreg{1}(clk, rst=rst)

        @select dir begin
            @when lc"0" begin
                dir_nxt = lc"1"
            end
            @others begin
                dir_nxt = lc"0"
            end
        end

        dir <| dir_nxt

        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(write=true, read=true)
        JuliaHDL.put_name!(dir, names_dict, buf, lang)
        
        @test issubset([clk, rst, dir, dir_nxt], keys(names_dict))
        name_dir = names_dict[dir]
        name_mux = names_dict[dir_nxt]
        
        s = read(seekstart(buf), String)
        sns_lst_reg = [clk,rst,dir_nxt]
        @test occursin(JuliaHDL.INDENT_TOKEN * name_dir * "_proc : process(" * join((names_dict[t] for t in sns_lst_reg), ", ") * ")\n" *
                        JuliaHDL.INDENT_TOKEN * "begin\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "if " * names_dict[rst] * " = '0' then\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[dir] * " <= " * JuliaHDL.get_const_val_str(dir.init, lang) * ";\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "elsif rising_edge(" *names_dict[clk] * ") then\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[dir] * " <= " * names_dict[dir_nxt] * ";\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end if;\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name_dir * "_proc;\n", s)

        sns_lst_mux = [dir]
        @test occursin(JuliaHDL.INDENT_TOKEN * name_mux * "_proc : process(" * join((names_dict[t] for t in sns_lst_mux), ", ") * ")\n" *
                       JuliaHDL.INDENT_TOKEN * "begin\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "case " * names_dict[dir] * " is\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "when '0' =>\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[dir_nxt] * " <= '1';\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "when others =>\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[dir_nxt] * " <= '0';\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end case;\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name_mux * "_proc;\n", s)

    end

    struct TestComponent{A, N} <: AbstractComponent{A}
        inp1_i::Usig{N}
        inp2_i::UsigVec{N, 5}
        inp3_i::UsigMat{N, 5, 5}
        outp1_o::Usig{N}
        outp2_o::UsigVec{N, 5}
        outp3_o::UsigMat{N, 5, 5}

        function TestComponent(N)
            inp1 = Usig(N)
            inp2 = SigVec(Usig(N) for _=1:5)
            inp3 = SigMat(Usig(N) for _=1:5, _=1:5)
            
            outp1 = Usig(inp1)
            outp2 = SigVec(Usig(p) for p in inp2)
            outp3 = SigMat((inp3[I] |> Usig) for I in (inp3 |> CartesianIndices))
            return new{:arch, N}(inp1, inp2, inp3, outp1, outp2, outp3)
        end
    end

    struct TestComponentBad1{A} <: AbstractComponent{A}
        a_t::Wire
    end
    struct TestComponentBad2{A} <: AbstractComponent{A}
        a::Wire
    end

    @testset "Port generation" begin
        N = 5
        test_comp = TestComponent(N)
        name = string(typeof(test_comp).name.name)
        buf = IOBuffer(write=true, read=true)
        JuliaHDL.print_header_pre(buf, name, lang)  
        JuliaHDL.generate_ports(test_comp, buf, lang)
        JuliaHDL.print_header_post(buf, name, lang)  
        s = read(seekstart(buf), String)


        @test occursin("entity " * name * " is\n", s)
        @test occursin("port (\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN * "inp1 : in unsigned($(N-1) downto 0);\n", s)
        @test occursin(join((JuliaHDL.INDENT_TOKEN * "inp2_$i : in unsigned($(N-1) downto 0)" for i = 1:5), ";\n") * ";\n", s)
        @test occursin(join((JuliaHDL.INDENT_TOKEN * "inp3_$i$j : in unsigned($(N-1) downto 0)" for i = 1:5, j = 1:5), ";\n") * ";\n", s)
        @test occursin(JuliaHDL.INDENT_TOKEN * "outp1 : out unsigned($(N-1) downto 0);\n", s)
        @test occursin(join((JuliaHDL.INDENT_TOKEN * "outp2_$i : out unsigned($(N-1) downto 0)" for i = 1:5), ";\n") * ";\n", s)
        @test occursin(join((JuliaHDL.INDENT_TOKEN * "outp3_$i$j : out unsigned($(N-1) downto 0)" for i = 1:5, j = 1:5), ";\n") * "\n", s)
        @test occursin(");\n", s)
        @test occursin("end " * name * ";\n", s)

        @test_throws ErrorException JuliaHDL.generate_ports(TestComponentBad1{:arch}(Wire()), buf, lang)
        @test_throws ErrorException JuliaHDL.generate_ports(TestComponentBad2{:arch}(Wire()), buf, lang)
    end

    @testset "Arch generation" begin
        N = 5
        test_comp = TestComponent(N)
        buf = IOBuffer(write=true, read=true)
        JuliaHDL.generate_architecture(test_comp, buf, lang)
        s = read(seekstart(buf), String)

        @test occursin("architecture " * string(typeof(test_comp).parameters[1]) * " of " * 
                        string(typeof(test_comp).name.name) * " is\n", s)

        @test occursin(" <= inp1;\n", s)
        @test all( occursin(" <= inp2_$i;\n", s) for i = 1:5 )
        @test all( occursin(" <= inp3_$i$j;\n", s) for i = 1:5, j = 1:5 )

        @test occursin("outp1 <= ", s)
        @test all( occursin("outp2_$i <= ", s) for i = 1:5 )
        @test all( occursin("outp3_$i$j <= ", s) for i = 1:5, j = 1:5 )

        @test occursin("begin\n", s)                        
        @test occursin("end arch;\n", s)
    end
end