if !all( isdefined.(Ref(Main), [:JuliaHDL, :Test]) )
    using JuliaHDL, Test
end

@testset verbose=true "Compound tests" begin
    lang = VHDL_Lang()
    a, b, c, d = Usig(5), Usig(5), Usig(5), Usig(6)
    i = Isig()
    j = Isig{1:5}(3)

    @testset verbose=true "Registers" begin
        clk ,rst = Wire(), Wire()
        
        @testset "Simple reg tests" begin
            ar = Reg(clk, a, rst=rst)
            @test ar isa Reg{Unsigned, 5}
            @test ar.from === a
            @test ar.blst === nothing

            ar2 = Reg{Unsigned, 5}(clk, rst=rst)
            @test ar2.from === nothing
            @test ar2.blst === nothing

            ir = Ireg(clk, rst=rst)
            @test ir.from === nothing
            @test ir.blst === nothing

            ir2 = Ireg(clk, init=0)
            @test ir2.from === nothing
            @test ir2.blst === nothing
            @test ir2.init === ic(0)
        end

        @testset "Pipeline tests" begin
            pipeline = 5
            ar = Reg(clk, a, pipeline)
            @test ar isa SigVec{Unsigned, length(a), pipeline}
            @test all(r.blst == ar && r isa Reg{Unsigned, length(a)} for r in ar)
            for i = 1:pipeline-1
                @test ar[i].from === ar[i+1]
            end
            @test ar[end].from === a

            ar2 = Reg(clk, a)
            @test ar2 isa Reg{Unsigned, length(a)}
            @test ar2.from === a
            @test ar2.blst === nothing

            ar3 = Reg(ar2)
            @test ar3.from === ar2 && ar3.clk === ar2.clk && ar3.rst === ar2.rst
            @test ar2.blst === ar3.blst !== nothing
            @test ar2 in ar2.blst && ar3 in ar3.blst
        end

        @testset "Multiple reg bundle tests" begin
            ar, br, cr, dr = Reg(clk, a, b, c, d, rst=rst)

            @test all(r isa Reg{Unsigned, 5} for r in (ar,br,cr))
            @test dr isa Reg{Unsigned, length(d)}
            @test all(r.from === p for (r, p) in zip((ar,br,cr,dr), (a,b,c,d)))
            blst = ar.blst
            @test blst isa Vector{Reg}
            @test all(r.blst == blst for r in (br,cr,dr))

            allr = Reg(clk, a, b, c, d, rst=rst)
            @test length(allr) == 4
            blst = allr[1].blst
            @test blst == allr
            @test blst isa Vector{Reg}
            @test allr isa Vector{Reg{Unsigned}}
        end

        @testset "Reg bundle generated code (no reset)" begin
            ar, br, cr, dr = Reg(clk, a, b, c, d)
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(ar, names_dict, buf, lang)
            name = names_dict[ar]

            @test all(r in keys(names_dict) for r in [ar,br,cr,dr,a,b,c,d,clk])
            
            s = read(seekstart(buf), String)
            sns_lst = [clk,a,b,c,d]
            @test occursin(JuliaHDL.INDENT_TOKEN * name * "_proc : process(" * join((names_dict[t] for t in sns_lst), ", ") * ")\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "begin\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "if rising_edge(" *names_dict[clk] * ") then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end if;\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name * "_proc;\n", s)

            blst = ar.blst
            for reg in blst
                @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[reg] * " <= " * names_dict[reg.from] * ";\n", s)
            end
        end

        @testset "Reg bundle generated code" begin
            ar, br, cr, dr = Reg(clk, a, b, c, d, rst=rst)
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(ar, names_dict, buf, lang)
            name = names_dict[ar]

            @test all(r in keys(names_dict) for r in [ar,br,cr,dr,a,b,c,d,clk,rst])
            
            s = read(seekstart(buf), String)
            sns_lst = [clk,rst,a,b,c,d]
            @test occursin(JuliaHDL.INDENT_TOKEN * name * "_proc : process(" * join((names_dict[t] for t in sns_lst), ", ") * ")\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "begin\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "if " * names_dict[rst] * " = '0' then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "elsif rising_edge(" *names_dict[clk] * ") then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end if;\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name * "_proc;\n", s)

            blst = ar.blst
            for reg in blst
                @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[reg] * " <= " * JuliaHDL.get_const_val_str(reg.init, lang) * ";\n", s)
                @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[reg] * " <= " * names_dict[reg.from] * ";\n", s)
            end
        end

        @testset "Reg single register gen. code" begin
            ar = Reg(clk, a, rst=rst)
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(ar, names_dict, buf, lang)
            name = names_dict[ar]

            @test all(r in keys(names_dict) for r in [ar,a,clk,rst])
            
            s = read(seekstart(buf), String)
            sns_lst = [clk,rst,a]
            @test occursin(JuliaHDL.INDENT_TOKEN * name * "_proc : process(" * join((names_dict[t] for t in sns_lst), ", ") * ")\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "begin\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "if " * names_dict[rst] * " = '0' then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "elsif rising_edge(" *names_dict[clk] * ") then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end if;\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name * "_proc;\n", s)

            @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[ar] * " <= " * JuliaHDL.get_const_val_str(ar.init, lang) * ";\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[ar] * " <= " * names_dict[a] * ";\n", s)
        end

        @testset "Reg loop" begin
            r1, r2 = RegLoop(clk, lc"00", 0ic{0:3}, rst=rst) do r1, r2
                r1 = r1 & lc"01"
                r2 = Cond(r2 < 3 => r2 + 1, nothing => 0ic{0:3})

                r1,r2
            end

            @test r1.from === r1 & lc"01"
            @test r2.from === Cond(r2 < 3 => r2 + 1, nothing => 0ic{0:3})
            @test r1.blst == r2.blst && [r1, r2] ⊆ r1.blst 
        end
    end

    @testset verbose=true "Muxes" begin
        sel = Lsig(2)

        m1 = Mux(nothing, sel, "00" => a + b, "01" => a * b)
        m2 = Mux(m1, sel, "00"^lc => a * b, "01"^lc => a + b, nothing => a * b)
        
        blst = m1.blst
        @test blst !== nothing && m2.blst === blst
        @test length(blst) == 2 && m1 in blst && m2 in blst
        @test length(m1.from) == 2 && length(m2.from) == 3
        @test m1.from[1].first === "00"^lc && m1.from[2].first === "01"^lc
        @test m2.from[1].first === "00"^lc && m2.from[2].first === "01"^lc && m2.from[3].first === nothing
        
        @test_throws MethodError m3 = Mux(m1, sel, "00"^uc => a * b) #sel is logical, not unsigned
        @test_throws ArgumentError m3 = Mux(m2, sel, "00"^lc => a + b, "01"^lc => a * b) #Redundant case entry
        @test_throws ArgumentError q = Mux(sel, "00"^lc => a * b, "01"^lc => a + b) #must have default case
        @test_throws ArgumentError q = Mux(sel, nothing => a+b, "00"^lc => a * b, "01"^lc => a + b) #default case must be last
        p = Mux(sel, "00"^lc => a * b, "01"^lc => a + b, nothing => a + b)
        @test_throws ArgumentError m3 = Mux(p, sel, "00"^lc => a + b, "01"^lc => a * b) #Can't bundle a singleton mux
        q = Mux(sel, "00"^lc => a * b, "01"^lc => a + b, nothing => a + b)
        @test p === q

        r = Mux(sel, "00" => a * b, "01" => a + b, nothing => a + b)
        @test p === r

        m3 = Mux(m2, sel, "00" => a * b + a, "01" => b + a * b, nothing => a * a)
        @test m3.from[1].first === "00"^lc && m3.from[2].first === "01"^lc && m3.from[3].first === nothing
        @test m3.from[1].second === (a*b+a) && m3.from[2].second === (b+a*b) && m3.from[3].second === (a*a)     
        @test m3.blst == blst   
        m4 = Mux(m2, sel, ["11", "01", nothing] .=> [a * b + a, b + a * b, a * a])
        @test m4.blst == blst
        @test m4.from[1].first === "11"^lc && m4.from[2].first === "01"^lc && m4.from[3].first === nothing
        @test m4.from[1].second === (a*b+a) && m4.from[2].second === (b+a*b) && m4.from[3].second === (a*a)


        m5 = Mux(m1, sel, "00"^lc => i, "01"^lc => 5ic)
        m6 = Mux(m3, sel, "00"^lc => j)
        m7 = Mux(m4, sel, "01"^lc => i, "00"^lc => j)
        @test range(m7) === range(i)
        @test length(blst) === 7

        @test_throws ArgumentError Mux(sel, "00"^lc => i, "01"^lc => 5ic)
        @test_throws ArgumentError Mux(sel,  nothing => i, "00"^lc => i, "01"^lc => 5ic)
        m8 = Mux(sel, "00"^lc => 0ic{0:0}, "01"^lc => 5ic{0:6}, nothing => 0ic{-5:5})
        @test m8.blst === nothing
        @test m8.from[1].first === "00"^lc && m8.from[1].second === 0ic{0:0}
        @test m8.from[2].first === "01"^lc && m8.from[2].second === 5ic{0:6}
        @test m8.from[3].first === nothing && m8.from[3].second  === 0ic{-5:5}
        @test range(m8) === -5:6

        isel = Isig{1:3}(1)
        m9 = Mux(isel, 1 => a, 2 => b, 3 => a + b, nothing => a - b)
        @test m9.from[1].first === 1ic{1:3} && m9.from[1].second === a
        @test m9.from[2].first === 2ic{1:3} && m9.from[2].second === b
        @test m9.from[3].first === 3ic{1:3} && m9.from[3].second  === a + b
        @test m9.from[4].first === nothing && m9.from[4].second  === a - b

        @testset "Mux bundle generated code" begin
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(m1, names_dict, buf, lang)
            name = names_dict[m1]
            
            s = read(seekstart(buf), String)
            arr = [sel, a + b, a * b, a * b + a, b + a * b, a * a, i, j]
            @test occursin(JuliaHDL.INDENT_TOKEN * name * "_proc : process(" * join((names_dict[t] for t in arr), ", ") * ")\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "begin\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "case " * names_dict[sel] * " is\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end case;\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name * "_proc;\n", s)

            cases = union((getindex.(m.from, 1) for m in blst)...)
            for case in cases
                if case !== nothing
                    @test occursin(JuliaHDL.INDENT_TOKEN^2 * "when " * JuliaHDL.get_const_val_str(case, lang) * " =>\n", s)
                else
                    @test occursin(JuliaHDL.INDENT_TOKEN^2 * "when others =>\n", s)
                end
            end
            for t in blst
                for (_, p) in t.from
                    if p isa ConstSig
                        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[t] * " <= " * JuliaHDL.get_const_val_str(p, lang) * ";\n", s)
                    else
                        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[t] * " <= " * names_dict[p] * ";\n", s)
                    end
                end
            end
        end

        @testset "Mux single generated code" begin
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(r, names_dict, buf, lang)
            name = names_dict[p]
            s = read(seekstart(buf), String)

            @test occursin(JuliaHDL.INDENT_TOKEN * "with " * names_dict[r.sel] * " select " * name * " <=\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[a * b] * " when " * JuliaHDL.get_const_val_str(lc"00", lang) * ",\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[a + b] * " when " * JuliaHDL.get_const_val_str(lc"01", lang) * ",\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[a + b] * " when others;\n", s)
        end

        @testset verbose=true "Mux (vector)" begin
            as, bs = SigVec(Usig(4) for _=1:5), SigVec(Usig(4) for _=1:5)
            is, js = SigVec(Isig(4) for _=1:5), SigVec(Isig{1:5}(3) for _=1:5)
            cs = Mux(nothing, sel, lc"00" => as, lc"01" => bs)
            ds = Mux(cs[1], sel, lc"00" => is, lc"01" => js)
    
            @test cs isa UsigVec{4, 5}
            @test ds isa IsigVec{range(is[1]), 5}
    
            for (c, d, ak, bk, ik, jk) in zip(cs, ds, as, bs, is, js)
                @test c.blst === d.blst === cs[1].blst
                @test c.from[1][1] === lc"00" && c.from[1][2] === ak
                @test c.from[2][1] === lc"01" && c.from[2][2] === bk
                @test d.from[1][1] === lc"00" && d.from[1][2] === ik
                @test d.from[2][1] === lc"01" && d.from[2][2] === jk
            end
        end
    end

    @testset verbose=true "Conditionals" begin
        d = Cond(nothing, a > b => a, eq(a, b) => a + b, nothing => b)
        e = Cond(d, a > b => b, eq(a, b) => a * b, nothing => a)

        blst = d.blst
        @test blst !== nothing && e.blst === blst
        @test length(blst) == 2 && d in blst && e in blst
        @test d.from[1].first == (a > b) && d.from[2].first === eq(a, b) && d.from[3].first === nothing
        @test e.from[1].first == (a > b) && e.from[2].first === eq(a, b) && e.from[3].first === nothing
        @test d.from[1].second == a && d.from[2].second == a + b && d.from[3].second == b
        @test e.from[1].second == b && e.from[2].second == a * b && e.from[3].second == a

        @test_throws ErrorException f = Cond(nothing, nothing => b, a > b => a, eq(a, b) => a+b) #nothing must be last case
        @test_throws ErrorException f = Cond(nothing, nothing => b, eq(a, b) => a+b, nothing => a) #can't have 2 nothings
        @test_throws ErrorException f = Cond(d, nothing => b, a > b => a, eq(a, b) => a+b) #nothing must be last case
        @test_throws ErrorException f = Cond(d, nothing => b, eq(a, b) => a+b, nothing => a) #can't have 2 nothings
        @test_throws ErrorException f = Cond(d, eq(a, b) => a, a > b => b) #inconsistent list of booleans
        @test_throws ErrorException f = Cond(e, nothing => a) #Must have same nr cases as in bundle
        @test_throws ErrorException f = Cond(e, a > b => b, eq(a, b) => a * b, a < b => a) #Must have same cases as in bundle
        @test_throws ErrorException f = Cond(nothing => b, a > b => a, eq(a, b) => a+b) #nothing must be last case
        @test_throws ErrorException f = Cond(nothing => b, eq(a, b) => a+b, nothing => a) #can't have 2 nothings
        @test_throws ErrorException f = Cond(eq(a, b) => a, a > b => b) #nothing is missing

        f = Cond(a > b => a, eq(a, 5) => a + b, nothing => b)
        @test f.blst === nothing
        @test f.from[1].first == (a > b) && f.from[2].first == eq(a, 5) && f.from[3].first === nothing
        @test f.from[1].second == a && f.from[2].second == (a + b) && f.from[3].second == b

        @test_throws ErrorException g = Cond(f, [a > b, nothing] .=> [b, a]) #f can't be bundled 

        g = Cond((a > b, eq(a, 5), nothing) .=> (a, a + b, b))
        @test g === f #This is why f can't be bundled 
        @test_throws ErrorException h = Cond(d, [a > b => a, eq(a, b) => a + b, nothing => b]) #Redundant

        h = Cond(d, a > b => i, eq(a, b) => j, nothing => 0ic)
        h2 = Cond(nothing, a > b => i, eq(a, b) => j, nothing => nothing)

        @testset "Cond bundle generated code" begin
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(d, names_dict, buf, lang)
            name = names_dict[d]
            
            s = read(seekstart(buf), String)
            arr = [a > b, a, b, i, eq(a, b), a+b, a*b, j]
            @test occursin(JuliaHDL.INDENT_TOKEN * name * "_proc : process(" * join((names_dict[t] for t in arr), ", ") * ")\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "begin\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "if " * names_dict[a > b] * " then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "elsif " * names_dict[eq(a, b)] * " then\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "else\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * "end if;\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN * "end process " * name * "_proc;\n", s)

            for t in blst
                for (_, p) in t.from
                    if p isa JuliaHDL.ConstSig
                        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[t] * " <= " * JuliaHDL.get_const_val_str(p, lang) * ";\n", s)
                    else
                        @test occursin(JuliaHDL.INDENT_TOKEN^3 * names_dict[t] * " <= " * names_dict[p] * ";\n", s)
                    end
                end
            end
        end

        @testset "Cond single generated code" begin
            names_dict = Dict{AbstractSignal, String}()
            buf = IOBuffer(write=true, read=true)
            JuliaHDL.put_name!(f, names_dict, buf, lang)
            s = read(seekstart(buf), String)

            @test occursin(JuliaHDL.INDENT_TOKEN * names_dict[f] * " <=\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[a]  * " when "* names_dict[a > b] * " else\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[a+b]  * " when " * names_dict[eq(a, 5)] * " else\n", s)
            @test occursin(JuliaHDL.INDENT_TOKEN^2 * names_dict[b] * ";\n", s)
        end

        @testset "Conditionals (integer)" begin
            i = Isig{0:5}()
            j = Isig()
            c = Cond(nothing, i > 0 => a, nothing => b)
    
            @test c.from[1][1] === (i > 0ic{0:5})
            @test c.from[1][2] === a
            @test c.from[2][1] === nothing
            @test c.from[2][2] === b
    
            @test_throws ErrorException Cond(c, j < 10 => a, nothing => b)
            d = Cond(j < 10 => a, nothing => b)
    
            @test d.from[1][1] === (j < 10)
            @test d.from[1][2] === a
            @test d.from[2][1] === nothing
            @test d.from[2][2] === b
        end
    
        @testset verbose=true "Conditionals (vector)" begin
            as, bs = SigVec(Usig(4) for _=1:5), SigVec(Usig(4) for _=1:5)
            is, js = SigVec(Isig(4) for _=1:5), SigVec(Isig{1:5}(3) for _=1:5)
            cs = Cond(nothing, a > b => as, nothing => bs)
            ds = Cond(cs[1], a > b => is, nothing => js)
    
            @test cs isa UsigVec{4, 5}
            @test ds isa IsigVec{range(is[1]), 5}
    
            for (c, d, ak, bk, ik, jk) in zip(cs, ds, as, bs, is, js)
                @test c.blst === d.blst === cs[1].blst
                @test c.from[1][1] === (a > b) && c.from[1][2] === ak
                @test c.from[2][1] === nothing && c.from[2][2] === bk
                @test d.from[1][1] === (a > b) && d.from[1][2] === ik
                @test d.from[2][1] === nothing && d.from[2][2] === jk
            end
        end
    end
end 