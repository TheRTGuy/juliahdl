for mod in [:JuliaHDL, :Test]
    if !isdefined(Main, mod)
        @eval using $mod
    end
end

@testset verbose=true "General primitive tests" begin
    @testset verbose=true "Signal constructors" begin
        @testset "Basic" begin
            N = 5
            a, b, c, d, w, e = Usig(N), Lsig(N), Ssig(N), Isig(), Wire(), Boolean()

            @test a.from === b.from === c.from === d.from === e.from === w.from === nothing
            @test a isa Usig{N} && a isa Signal{Unsigned, N}
            @test Usig{N} === AbstractSignal{Unsigned,N}
            @test b isa Lsig{N} && b isa Signal{Logical, N} 
            @test Lsig{N} === AbstractSignal{Logical,N}
            @test c isa Ssig{N} && c isa Signal{Signed, N}
            @test Ssig{N} === AbstractSignal{Signed,N}
            @test d isa Isig{Int64(typemin(Int32)):Int64(typemax(Int32))} && d isa Signal{Integer, Int64(typemin(Int32)):Int64(typemax(Int32))}
            @test Isig{Int64(typemin(Int32)):Int64(typemax(Int32))} === AbstractSignal{Integer,Int64(typemin(Int32)):Int64(typemax(Int32))}
            @test w isa Wire && w isa Signal{Logical, 1}
            @test Wire === AbstractSignal{Logical, 1}
            @test e isa Boolean && e isa Signal{Bool, 1}

            @test isabstracttype(Usig)
            @test isabstracttype(Ssig)
            @test isabstracttype(Lsig)
            @test isabstracttype(Isig)
            @test isabstracttype(Wire)
            @test isabstracttype(Boolean)
        end

        @testset "Advanced" begin
            init = "01010"
            N = length(init)
            a, b, c = Usig(init), Lsig(init), Ssig(init)
            @test a isa Usig{N} && a isa Signal{Unsigned, N}
            @test b isa Lsig{N} && b isa Signal{Logical, N} 
            @test c isa Ssig{N} && c isa Signal{Signed, N}
            @test a.init === b.init === c.init
            @test a.from === b.from === c.from === nothing
            @test_throws ArgumentError Usig("f0") #Illegal characters
            @test_throws ArgumentError Lsig("f0")
            @test_throws ArgumentError Ssig("f0")

            i = Isig(5, range=1:5)
            @test i isa Isig{1:5} && i isa Signal{Integer, 1:5}
            @test i.init === 5
            @test_throws ArgumentError Isig{1:3:5}() #not unit range
            @test_throws ArgumentError Isig{5:1}() #range not increasing
            @test_throws ArgumentError Isig{1:5}(6) #outside range
        end

        @testset "Conversions" begin
            N = 5
            bits_sigs = zip([Usig, Ssig, Lsig],[Unsigned, Signed, Logical])
            #First test bits signal conversions
            for (T,P) in bits_sigs, (S,Q) in bits_sigs
                if P != Q #no need to test the same type conversion
                    t = T(N)
                    s = S(t) #T -> S
                    @test s isa S{N} && s isa JuliaHDL.SigConverter{Q, N} <: AbstractSignal
                    @test length(s) == N
                    @test s.from === t
                    @test S(t) === s 
                else
                    t = T(N)
                    s = Signal(t)
                    @test s.from === t
                    @test s isa Signal{P, N}
                end
            end

            #Test integer to numeric bits signal
            i = Isig(5)
            u, s = Usig{N}(i), Ssig{N}(i)
            @test u isa Usig{N} && length(u) == N
            @test s isa Ssig{N} && length(s) == N
            
            #can't convert integer to numeric bits signal without specifying bit length
            @test_throws MethodError Usig(i) 
            @test_throws MethodError Ssig(i)
            @test_throws MethodError Lsig(i)
            #can't convert integer to logical bit signal
            @test_throws MethodError Lsig{N}(i) 

            #back to integer
            let u=u,s=s
                u, s = Isig(u), Isig(s)
                @test u isa Isig{Int64(typemin(Int32)):Int64(typemax(Int32))}
                @test s isa Isig{Int64(typemin(Int32)):Int64(typemax(Int32))}
            end
            #back to integer (custom range)
            let u=u,s=s
                u, s = Isig{1:5}(u), Isig{1:5}(s)
                @test u isa Isig{1:5}
                @test s isa Isig{1:5}
            end

            #can't convert logical bit signal to integer
            @test_throws MethodError Isig(Lsig(N)) 
            #bounds reversed
            @test_throws ArgumentError Isig{5:1}(u)
            @test_throws ArgumentError Isig{1:3:5}(u)

            #to another integer
            j = Isig{1:5}(i)
            @test j isa Isig{1:5} && length(j) == 5 && j isa JuliaHDL.SigConverter{Integer,1:5}
            #bounds reversed
            @test_throws ArgumentError Isig{5:1}(i)
            @test_throws ArgumentError Isig{1:3:5}(i)
            k = Signal(i)
            @test k.from === i && k isa Signal{Integer, Int64(typemin(Int32)):Int64(typemax(Int32))}
        
            w = Wire()
            b = Boolean()
            e = Boolean(w)
            f = Wire(b)
            @test e.from === w && e isa JuliaHDL.SigConverter{Bool,1}
            @test f.from === b && f isa JuliaHDL.SigConverter{Logical,1}
        end
    end
    @testset "Signal vector" begin
        v = [Usig(5) for _=1:3]
        a, b, c = v
        vec = SigVec(v) #default vector arg constructor
        vec2 = SigVec(a, b, c) #varg constructor
        vec3 = SigVec(p for p in v) #generator constructor


        @test vec isa UsigVec{5,3} <: AbstractVector{Usig{5}}
        @test vec === vec2 === vec3 #SigVec is a immutable type
        @test length(vec) == 3

        #Signals must be same bit length
        @test_throws MethodError SigVec([Usig(i) for i = 2:4])
        @test_throws MethodError SigVec(Usig(i) for i = 2:4)
        @test_throws MethodError SigVec(Usig(3), Usig(4), Usig(5))
        #Signal must have same underlying type
        @test_throws MethodError SigVec([Signal{S}("00") for S in [Signed, Unsigned, Logical]])
        @test_throws MethodError SigVec(Signal{S}("00") for S in [Signed, Unsigned, Logical])
        @test_throws MethodError SigVec((Signal{S}("00") for S in [Signed, Unsigned, Logical])...)
    end
    @testset "Signal matrix" begin
        v = [Usig(5) for _=1:3, _=1:3]
        mat = SigMat(v) #default vector arg constructor
        mat2 = SigMat(p for p in v) #generator constructor

        @test mat isa UsigMat{5,3} <: AbstractMatrix{Usig{5}}
        @test mat === mat2 #SigMat is a immutable type
        @test size(mat) === (3,3)
    end

    @testset "Constants" begin
        a, b, c = lc("00"), uc("01"), sc("10")
        u, v, w = Lsig(2), Usig(2), Ssig(2)
        @test a.val isa String && b.val isa String && c.val isa String
        @test a === "00"^lc === "00"^u
        @test b === "01"^uc === 1uc{2} === 1^v
        @test c === "10"^sc === 2sc{2} === 2^w
        @test_throws MethodError 1^uc #Must specify bitwidth
        @test_throws MethodError 1^sc
        @test_throws MethodError 1^lc{2} #converting integer to logical signal not allowed
        @test_throws ArgumentError "001"^u #Length of string litral must match signal bitwidth
        @test_throws ArgumentError "001"^v
        @test_throws ArgumentError "001"^w
        @test_throws ArgumentError "0f"^u #contains illegal character
        @test_throws ArgumentError "0f"^v #contains illegal character
        @test_throws ArgumentError "0f"^w #contains illegal character

        i = ic{0:5}(5)
        j = Isig{0:5}()
        @test i.val isa Integer && i.val === 5
        @test i === 5^j === 5ic{0:5}
        @test_throws ArgumentError ic{5:0}(5)
        @test_throws ArgumentError ic{0:5}(6)
        @test_throws ArgumentError ic{5:-1:1}(4)
        @test_throws ArgumentError "00"^i

        b = bc(true)
        f = Boolean()
        @test b.val
        @test true^f === b
        @test_throws MethodError 1^f
        @test_throws MethodError "0"^f
    end
end

@testset verbose=true "Code generation (VHDL)" begin
    @testset "Signal conversion and buffers" begin
        N = 5
        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(UInt8[], write=true)
        lang = VHDL_Lang()

        bits_sigs = zip([Usig, Ssig, Lsig],[Unsigned, Signed, Logical])
        for (T, S) in bits_sigs
            a = T(5)
            b = Signal(a)
            JuliaHDL.put_name!(a, names_dict, buf, lang)
            JuliaHDL.put_name!(b, names_dict, buf, lang)

            s = String(buf.data)
            @test !occursin(names_dict[a] * " <= ", s)
            @test occursin(names_dict[b] * " <= " * names_dict[a] * ";\n", s)
            for (P, Q) in bits_sigs 
                c = P(a)
                JuliaHDL.put_name!(c, names_dict, buf, lang)
                if Q !== typeof(a).parameters[1]
                    @test occursin(names_dict[c] * " <= " * JuliaHDL.TypeStr_VHDL[Q] * "(" * names_dict[a] * ");\n", String(buf.data))
                else
                    @test occursin(names_dict[c] * " <= " * names_dict[a] * ";\n", String(buf.data))
                end
            end

            if S != Logical
                i = Isig(a)
                d = T{N}(i)
                j = Isig{0:5}(i)
                JuliaHDL.put_name!(i, names_dict, buf, lang)
                JuliaHDL.put_name!(d, names_dict, buf, lang)
                JuliaHDL.put_name!(j, names_dict, buf, lang)
                s = String(buf.data)
                @test occursin(names_dict[i] * " <= to_" * JuliaHDL.TypeStr_VHDL[Integer] * "(" * names_dict[a] * ");\n", s)
                @test occursin(names_dict[d] * " <= to_" * JuliaHDL.TypeStr_VHDL[S] * "(" * names_dict[i] * ", $N);\n", s)
                @test occursin(names_dict[j] * " <= " * names_dict[i] * ";\n", s)
            end
        end
        
        w = Wire()
        b = Boolean()
        e = Boolean(w)
        f = Wire(b)
        JuliaHDL.put_name!(e, names_dict, buf, lang)
        JuliaHDL.put_name!(f, names_dict, buf, lang)
        s = String(buf.data)
        @test occursin(names_dict[e] * " <= " * names_dict[w] * " = '1';\n", s)
        @test occursin(names_dict[f] * " <= '1' when " * names_dict[b] * " else '0';\n", s)
    end
end