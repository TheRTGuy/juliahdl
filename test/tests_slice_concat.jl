if !all( isdefined.(Ref(Main), [:JuliaHDL, :Test]) )
    using JuliaHDL, Test
end

@testset "Test slice generator output" begin
    a = Usig(10)
    
    s1 = a[1 => 6]
    s2 = a[6 => 1]
    s3 = a[1:5]
    s4 = a[[1,2,3]]
    s5 = a[1 => 1]
    s6 = a[5]

    @test s1 isa Usig{6} && s1 isa JuliaHDL.SigSlice
    @test s2 isa Usig{6} && s2 isa JuliaHDL.SigSlice
    @test s3 isa Usig{5} && s1 isa JuliaHDL.SigSlice
    @test s4 isa Lsig{3} && s2 isa JuliaHDL.SigSlice
    @test s5 isa Wire && s1 isa JuliaHDL.SigSlice
    @test s6 isa Wire && s2 isa JuliaHDL.SigSlice
    @test a[0] isa Wire
    
    let (i, j) = (Isig(), Isig{1:5}(3))
        @test_throws MethodError i[1] #Can't slice integers
        @test_throws MethodError j[1]
    end
    for sl in (-1, 10, -1=>5, -1=>12, 0=>10, 
                -1:2:9, 4:-2:-2,[-1,0,1],[0,1,10],[-1,0,10],
                5=>-1, 12=>-1, 10=>0)
        @test_throws ArgumentError a[sl] #Out of range
    end

    @testset "Codegen (VHDL)" begin
        lang = VHDL_Lang()
        name = "Test"
        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(write=true, read=true)
        JuliaHDL.print_assignment!(s1, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(s2, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(s3, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(s4, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(s5, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(s6, name, names_dict, buf, lang)
    
        s = read(seekstart(buf), String)
    
        @test a in keys(names_dict) && length(names_dict) == 1
        @test all(s.from == a for s in [s1, s2, s3, s4, s5, s6])
        @test occursin(name * " <= $(names_dict[a])(1 to 6);", s)
        @test occursin(name * " <= $(names_dict[a])(6 downto 1);", s)
        @test occursin(name * " <= $(names_dict[a])(1 to 5);", s)
        @test occursin(name * " <= " * join(["$(names_dict[a])($(i))" for i in [1,2,3]] , " & ") * ";", s)
        @test occursin(name * " <= $(names_dict[a])(1);", s)
        @test occursin(name * " <= $(names_dict[a])(5);", s)
    end
end

@testset "Test concatenation" begin
    a, b, c = Usig(5), Usig(6), Usig(7)
    e, f = Isig(), Boolean()
    g, h = Lsig(3), Ssig(5)
    abc = Concatenator(a, b, c)
    agh = ^(a, g, h)
    ac = a ^ c

    
    @test length(abc) == length(a) + length(b) + length(c)
    @test abc isa Usig
    @test agh isa Lsig
    @test length(ac) == length(a) + length(c)
    @test length(agh) === length(a) + length(g) + length(h)
    @test ac isa Concatenator
    @test agh isa Concatenator
    
    @test typeof(abc).parameters[1] == Unsigned
    @test typeof(ac).parameters[1] == Unsigned
    
    i, j = Isig(), Isig{1:5}(3)
    @test_throws MethodError i ^ j #Can't concat integers 

    @testset "Codegen (VHDL)" begin
        lang = VHDL_Lang()
        name = "Test"
        names_dict = Dict{AbstractSignal, String}()
        buf = IOBuffer(write=true, read=true)
        JuliaHDL.print_assignment!(abc, name, names_dict, buf, lang)
        JuliaHDL.print_assignment!(ac, name, names_dict, buf, lang)
        
        s = read(seekstart(buf), String)
        
        @test all(p in keys(names_dict) for p in [a,b,c])
        @test occursin(name * " <= $(names_dict[a]) & $(names_dict[b]) & $(names_dict[c]);", s)
        @test occursin(name * " <= $(names_dict[a]) & $(names_dict[c]);", s)
    end
end