if !all( isdefined.(Ref(Main), [:JuliaHDL, :Test]) )
    using JuliaHDL, Test
end

struct TestComp{A, N, L, K} <: AbstractComponent{A}
    A_i::UsigMat{N, L, K}
    b_i::UsigVec{N, L}
    c_i::Usig{N}

    U_o::UsigMat{N, L, K}
    v_o::UsigVec{N, L}
    w_o::Usig{N}
end

struct TestComp_wrong1{A, N, L, K} <: AbstractComponent{A}
    w_j::Usig{N}
end
struct TestComp_wrong2{A, N, L, K} <: AbstractComponent{A}
    wo::Usig{N}
end
struct TestComp_wrong3{A, N, L, K} <: AbstractComponent{A}
    w_o::Int
end
struct TestComp_outonly{A, N, L, K} <: AbstractComponent{A}
    U_o::UsigMat{N, L, K}
    v_o::UsigVec{N, L}
    w_o::Usig{N}
end

@testset verbose=true "Tests portmap (VHDL)" begin
    N, L, K = 5, 2, 2
    A = SigMat(Usig(N) for _=1:L, _=1:K)
    b = SigVec(Usig(N) for _=1:L)
    c = Usig(N)

    U, v, w = PortMap(TestComp{:rtl, N, L, K}, :A_i => A, :b_i => b, :c_i => c)

    from = w.from
    blst = w.blst
    @test from === (:A_i => A, :b_i => b, :c_i => c)
    @test all(p in blst for p in (U, v, w))
    @test all(
        p.from === from && p isa PortMap{Unsigned, N} && p.blst == blst  for p in U
    )
    @test all(
        p.from === from && p isa PortMap{Unsigned, N} && p.blst == blst  for p in v
    )
    @test U isa UsigMat{N, L, K}
    @test v isa UsigVec{N, L}
    @test w isa PortMap{Unsigned, N}
    @test_throws ArgumentError PortMap(TestComp{N, L, K}, :A_i => A) #incompatible types (TestComp is missing the 'A' param)
    @test_throws ArgumentError PortMap(TestComp{:rtl}, :A_i => A, :b_i => b, :c_i => c) #no parameters
    @test_throws ArgumentError PortMap(TestComp{:rtl, N, L, K}, :U_o => U)
    @test_throws ArgumentError PortMap(TestComp{:rtl, N, L, K}, :Ai => A)
    @test_throws ArgumentError PortMap(TestComp{:rtl, N, L, K}, :A_ => A)
    @test_throws ArgumentError PortMap(TestComp{:rtl, N, L, K}, :A_j => A)
    @test_throws ErrorException PortMap(TestComp_wrong1{:rtl, N, L, K}) #w_j has no valid direction
    @test_throws ErrorException PortMap(TestComp_wrong2{:rtl, N, L, K}) #wo has no _ delimiter
    @test_throws ErrorException PortMap(TestComp_wrong2{:rtl, N, L, K}) #w_o has wrong type
    @test_throws ErrorException PortMap(TestComp_outonly{:rtl}) #No parameters
    @test_throws ErrorException PortMap(TestComp_outonly{:rtl, N}) #No parameters
    @test_throws ErrorException PortMap(TestComp_outonly{:rtl, N, L}) #No parameters
    pmap2 = PortMap(TestComp_outonly{:rtl, N, L, K})
    @test pmap2.w_o.from == ()
    
    names_dict = Dict{AbstractSignal, String}()
    buf = IOBuffer(write=true, read=true)
    JuliaHDL.put_name!(w, names_dict, buf, VHDL_Lang())
    s = read(seekstart(buf), String)
    namew = names_dict[w]
    
    @test occursin(JuliaHDL.INDENT_TOKEN * "TestComp_rtl_" * namew * " : entity " * w.lib * ".TestComp(rtl)\n", s)
    @test occursin(JuliaHDL.INDENT_TOKEN * "port map(\n", s)

    @test all( 
        occursin(JuliaHDL.INDENT_TOKEN^2 * "A_$i$j => " * names_dict[A[i, j]] * ",\n", s) for i=1:L, j=1:K
    )
    @test all( 
        occursin(JuliaHDL.INDENT_TOKEN^2 * "b_$i => " * names_dict[b[i]] * ",\n", s) for i=1:L
    )
    @test occursin(JuliaHDL.INDENT_TOKEN^2 * "c => " * names_dict[c] * ",\n", s)

    @test all(
        occursin(JuliaHDL.INDENT_TOKEN^2 * u.pname * " => " * names_dict[u] * ",\n", s) for u in U
    )
    @test all(
        occursin(JuliaHDL.INDENT_TOKEN^2 * u.pname * " => " * names_dict[u] * ",\n", s) for u in v
    )
    @test occursin(JuliaHDL.INDENT_TOKEN^2 * w.pname * " => " * names_dict[w] * ");\n", s)

end