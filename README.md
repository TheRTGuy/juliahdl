# Introduction
This is a Julia package that aims to allow describing and designing digital hardware by transpiling to a lower-level HDL, such as VHDL. The objective of the package is to ubiquitously turn Julia expressions into hardware-description expressions in a way that feels natural and Julia-esk. This is done by heavily exploiting the type system of Julia and MD, while enforcing a strict `signal-flow`-like design using pure Julia expressions.

# Motivation
One of the main motivations of making yet another DSL/transpiler for VHDL, but in Julia, is to take advantage of the many features the Julia language has to offer, such as its superb type system and built-in multiple dispatch. In particular, MD allows complex modular designs, e.g. where new IP blocks can be seemlessly plugged into an existing system. Also, in terms of the design of the transpiler, MD allows turning various primitive objects into HDL expressions during the transpilation process. This means that many existing functions and operations in Julia can be enabled on these expressions, by simply dispatching atomic operations on the primitives.

As an example, an unsigned 32-bit signal in VHDL is modeled as an `Usig{32}` object in Julia. Say that we construct two of these: `a, b = Usig(32), Usig(32)`, then generating a product expression object from these is as straightforward as `c = a * b`. Now, if we want to go further, and design a dot-product of vectors of unsigned 32-bit signals, then this can be written as simply as:
```Julia
as, bs = SigVec(Usig(32) for _=1:5), SigVec(Usig(32) for _=1:5)
dp = as'bs
```

Notice that this humble 2-liner is exactly how one would express this operation mathematically in Julia, which is another key motivation of building a HDL transpiler ontop of the Julia language: to abstract away the low-level design details and focus on the functional behavior*. In fact, one of the many reasons one would design their own custom IP in the first place, is to accelerate a numerical algorithm in hardware, which typically involves several design stages:
1. Create a functional prototype in some sort of numerical computing environment, e.g. in MATLAB.
2. Validate and refine the functional behavior of the algorithm via simulations.
3. Translate the design into hardware.
4. Validate the design and compare against the functional reference.
5. Repeat until design is complete.

Now, wouldn't it be nice, if all of these stages can be reduced to just 2? Well, if that would amount to just switching a couple of data types of the inputs and outputs of the algorithm, then it certainly is possible. Well, almost...

*Note however that here we're not performing the dot-product, we're "describing" it, and therefore generating an "expression" that would then be transpiled into HDL code.

# Sequential (software) vs parallel (hardware) design
A fundamental key difference when switching from algorithm design on a sequential platform to hardware, which is inherently parallel, is that we add more dimensions in the design space that we need to take care of. For example, if one chooses to implement the dot-product above as it is, then the generated expressions will lead to a fully combinatorial solution with 5 multipliers and 4 adders. While this design performs the dot-product in one clock cycle, it also requires quite a lot of resources, and limits the maximum clock frequency, due to combinatorial delay. 

Alternatively, we could choose to load the inputs serially, and multiply-accumulate them one by one in a register. Such a design may be written in "vanilla" Julia as
```Julia
function dot_prod(as::Vector{UInt32}, bs::Vector{UInt32})
    c = zero(UInt32)
    for i = 1:length(as)
        c = c + a[i]*b[i]
    end
    return c
end
```
The same algorithm looks quite different when written for hardware:
```Julia
function dot_prod_serial(N, clk::Wire(), rst::Wire(), start::Wire(),
                         a::Usig{32}, b::Usig{32})
    c = Ureg{32}(clk, rst=rst)
    i = Ireg{0:N-1}(clk, rst=rst)
    busy = Lreg{1}(clk, rst=rst)

    @select begin
        @when busy == lc"0" begin
            @select begin
                @when start = lc"1" begin
                    ctr_nxt = uc{32}(0)
                    i_nxt = 0
                    busy_nxt = lc"1"
                end
            end
        end
        @others begin
            @select begin
                @when i < N-1 begin
                    c_nxt = c + a * b
                    i_nxt = i + 1
                end
                @others busy_nxt = lc"0"
            end
        end
    end
    c <| c_nxt
    i <| i_nxt
    busy <| busy_nxt
    ready = busy & Wire(eq(ctr, N-1))

    return c, ready
end
```
Let's unpack this one: as inputs, we need the number of elements we want to multiply and add, the clock and reset signals for our registers, a `start` signal that starts the algorithm, and the vectors `a` and `b` that would be loaded serially via two 32-bit input signals. For the outputs, we need the accumulated sum, and a `ready` signal that tells external hardware that the result is ready to be consumed. Then, we define registers and some multiplexer logic that determine the next state of these registers. The multiplexer logic does the following: if the device is not busy, then we wait for a logic-hi on the `start` signal, in which case the device can enter busy mode. If the device is in busy mode, then we multiply the two inputs, and add the result to the previous value of `c`. The MAC op is then lodead into `c` on the next rising edge. We keep doing this into each "clock" zone, while also incrementing the `i` register, until the value of `i` reaches the maximum number of elements we want to multiply. Then, we stop and go back idle mode.

While both of these designs are pure Julia, they are still fundamentally different: in the first we are thinking sequentially and we're actually computing the dot-product, while in the latter, all of the Julia expressions serve to just construct a signal-flow graph that will then be transpiled to VHDL. It is thus assumed, that all of these expressions "execute" in parallel. We introduce sequential computation by pipe-lining the combinatorial paths using registers. This fundamental difference is why most numerical algorithms cannot be simply translated into a hardware design: one has to take care of how the time-space trade-off is exploited, and how the various components are connected to each other. In fact, this design is one of many that can implement the same functionality. For example, we could've chosen to load 4 inputs at a time instead of 2, or even more, resulting in a "hybrid" serial-parallel implementation. All of these design decisions have to be taken aposteriori, which is one of the key reasons why writing hardware code is hard, even in a high-level language such as Julia. Nevertheless, Julia does make it significantly easier and more natural than other DSL's, especially since one can integrate the functional prototyping and validation flow with the hardware design flow into one environment.

# On HLS (future work)
This topic of how to map an algorithm to a hardware description is in fact known as High-level synthesis (HLS). There are numerous books and papers on the subject, which highlights how complex HLS and its implementation is. Nevertheless, Julia can help tremendously in that area too, which brings us to the next key motivation to use Julia for hardware designs: its metaprogramming capability. Imagine in a world, where we could have a macro, say, `@hls` that we can apply to the purely functional Julia design, and which would then spit out the hardware design above, given some constraints? That is something definitely within the capabilities of Julia. In particular, its metaprogramming capabilities allow manipulating its own AST, which is something severely lacking in any other hardware DSLs and transpilers. The use of macros has actually already been demonstrated in the previous example, where they have been used to simplify the generation of conditional multiplexers.

HLS is not supported currently by this package, but the idea is that once all of the basic transpiler features are implemented, then one can build an HLS metaprogramming layer on top of this. 

# Simulation (future work)
Another key motivation to use Julia for hardware design is its speed. In particular, one of the goals for this project is to be able to simulate a hardware design directly in Julia. Now, since the design itself is ultimately specified in a signal-flow graph, then implementing a simulator becomes very natural in Julia, and I would expect that its inherent speed will greatly be appreciated once very complicated designs need to be simulated. Easier said than done of course.

