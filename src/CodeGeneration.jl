#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#


#Tokens
const INDENT_TOKEN = " "^4

get_arch(_::Type{<:AbstractComponent{A}}) where A = "$A"
get_arch(comp::AbstractComponent) = get_arch(typeof(comp))

const SigTypeStr = Dict(Unsigned => "Unsigned",
                    Signed => "Signed",
                    Logical => "Logic",
                    Bool => "Bool",
                    Integer => "Int")
const OPNAMES = Dict( 
    [:+, :-, :*, :/,
        :and, :or, :xor, :nand, :nor, :xnor, :not,
        :(=), :/=, :<=, :<, :>=, :>,
        :shift_left, :shift_right, :rotate_left, :rotate_right] .=>
    ["Add", "Sub", "Mul", "Div",
        "And", "Or", "Xor", "Nand", "Nor", "Xnor", "Not",
        "Eq", "Neq", "Leq", "Lt", "Geq", "Gt",
        "Shl", "Shr", "Rol", "Ror"]
)                    
function get_name(p::AbstractSignal{S}, n) where S
    return SigTypeStr[S] * "_$n"
end
get_name(p::Reg, n) = "Reg_$n"
get_name(p::Wire, n) = "Wire_$n"
get_name(m::Mux, n) = "Mux_$n"
get_name(c::Cond, n) = "Cond_$n"
get_name(conc::Concatenator, n) = "Concat_$n"
get_name(s::SigSlice, n) = "Slice_$n"
get_name(op::AbstractOperator, n) = OPNAMES[op.operator]*"_$n"

function print_assignment!(p::ConstSig, x, _, buf, lang)
    println(buf, INDENT_TOKEN, assign_op_str(x, lang), get_const_val_str(p, lang), expr_terminator_str(lang))
end

function put_name!(p::AbstractSignal, names_dict::Dict{AbstractSignal, String}, buf, lang)
    if p in keys(names_dict)
        name = names_dict[p]
        return name
    else
        if !isa(p, ConstSig)
            name = get_name(p, length(names_dict) + 1)
            push!(names_dict, p => name)
            print_assignment!(p, name, names_dict, buf, lang)
        else
            name = get_const_val_str(p, lang)
        end
        return name
    end
end

function print_assignment!(reg::Reg, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    #First, add of all regs in the bundle to the names_dict (except `reg`, it's already there):
    # this avoids calling this func on any of them in the future
    blst_str = [x]
    blst_init_str = [get_const_val_str(reg.init, lang)]
    if reg.blst === nothing
        blst = (reg,)
    else
        blst = reg.blst
        for r in reg.blst
            if r !== reg
                n = length(names_dict) + 1
                name = get_name(r, n)
                push!(names_dict, r => name)
                push!(blst_str, name)
                push!(blst_init_str, get_const_val_str(r.init, lang))
            end
        end
    end

    #Get names, put on dict, and print sensitivity list
    clk_name = put_name!(reg.clk, names_dict, buf, lang)
    slst_str = [clk_name]

    #Add reset if availabe to sensitivity list
    rst_name = nothing
    if reg.rst !== nothing
        rst_name = put_name!(reg.rst, names_dict, buf, lang)
        push!(slst_str, rst_name)
    end
    
    blst_from_str = String[]
    for r in blst
        if r.from !== nothing
            name = put_name!(r.from, names_dict, buf, lang)

            push!(blst_from_str, name)
            if !(r.from isa ConstSig) && !(name in slst_str)
                push!(slst_str, name)
            end
        else
            error("register with no driver.")
        end
    end

    #Print body of register(s)
    print_reg_body(x, reg.rising_edge, clk_name, rst_name, blst_str, blst_init_str, blst_from_str, slst_str, buf, lang)
end

function print_mux!(mux::Mux, ::Nothing, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    sel_name = put_name!(mux.sel, names_dict, buf, lang)

    names_lst, cases_lst = [], []
    for (case, p) in mux.from
        name_p = put_name!(p, names_dict, buf, lang)
        push!(names_lst, name_p)
        push!(cases_lst, case === nothing ? nothing : get_const_val_str(case, lang))
    end
    print_inline_mux_body(x, sel_name, names_lst, cases_lst, buf, lang)
end

function print_mux!(mux::Mux, blst::Vector{Mux}, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    @assert mux in keys(names_dict)
    
    #First, add of all muxes in the bundle to the names_dict (except `mux`, it's already there):
    # this avoids calling this func on any of them in the future
    for t in blst #For each mux output
        if t !== mux
            n = length(names_dict) + 1
            push!(names_dict, t =>  get_name(t, n))
        end
    end
    
    sel_name = put_name!(blst[1].sel, names_dict, buf, lang)

    #Get names, put on dict, and print sensitivity list
    slst_str = [sel_name]
    case_dict = Dict()
    
    #Next, we put and print the rest of the signals in names_dict,
    #complete the sensitivity list
    for t in blst 
        for (case, p) in t.from
            if p !== nothing
                name_p = put_name!(p, names_dict, buf, lang)
                if !(p isa ConstSig) && !(name_p in slst_str)
                    push!(slst_str, name_p)
                end

                case_name = case === nothing ? nothing : get_const_val_str(case, lang)

                if !(case_name in keys(case_dict)) #Not in the dictionary of cases
                    case_dict[case_name] = [(names_dict[t], name_p)]
                else
                    push!(case_dict[case_name], (names_dict[t], name_p))
                end
            end
        end
    end

    #Print body of process
    print_mux_body(x, sel_name, slst_str, case_dict, buf, lang)
end
function print_assignment!(mux::Mux, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    print_mux!(mux, mux.blst, x, names_dict, buf, lang)
end

function print_cond!(cond::Cond, _::Nothing, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    src_lst_str = []
    cond_lst_str = []
    for (b, p) in cond.from
        if b !== nothing 
            name_p = put_name!(p, names_dict, buf, lang)
            name_b = put_name!(b, names_dict, buf, lang)
            push!(src_lst_str, name_p)
            push!(cond_lst_str, name_b)
        end
    end
    name_p = put_name!(cond.from[end][2], names_dict, buf, lang)
    push!(src_lst_str, name_p)
    print_inline_cond_body(x, src_lst_str, cond_lst_str, buf, lang)
end

function print_cond!(cond::Cond, blst::Vector{Cond}, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    #Get names, put on dict, and print sensitivity list
    slst_str = []
    cond_src_lst = []

    #First, add all conditionals in the bundle to the names_dict (except `cond`, it's already there):
    # this avoids calling this func on any of them in the future
    for t in blst #For each mux output
        if t !== cond
            n = length(names_dict) + 1
            push!(names_dict, t => get_name(t, n))
        end
    end

    #Then we put and print the rest of the signals in names_dict,
    #complete the sensitivity list, and we build up the case dict
    for (i, (b, _)) in enumerate(cond.from)        
        if b !== nothing
            name_b = put_name!(b, names_dict, buf, lang)
            push!(slst_str, name_b)
        else
            name_b = nothing
        end
        src_lst_str = []
        for t in blst 
            p = t.from[i][2]
            if p !== nothing
                name_p = put_name!(p, names_dict, buf, lang)
                push!(src_lst_str, (name_p, names_dict[t]))
                if !(p isa ConstSig) && !(name_p in slst_str)
                    push!(slst_str, name_p)
                end
            end
        end
        push!(cond_src_lst, name_b => src_lst_str)
    end
    print_cond_body(x, slst_str, cond_src_lst, buf, lang)
end
function print_assignment!(cond::Cond, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    print_cond!(cond, cond.blst, x, names_dict, buf, lang)
end

function print_assignment!(pmap::PortMap, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    comp_name = "$(pmap.t.name.name)"
    arch_name = "$(get_arch(pmap.t))"
    port_lst = []

    #inputs first
    for (pname, p) in pmap.from
        pname, _ = rsplit(String(pname), "_"; limit=2)

        if p isa SigVec
            for (i, s) in enumerate(p)
                name_p = put_name!(s, names_dict, buf, lang) 
                push!(port_lst, "$(pname)_$i" => name_p)
            end
        elseif p isa SigMat
            for ij in CartesianIndices(p)
                name_p = put_name!(p[ij], names_dict, buf, lang) 
                push!(port_lst, "$(pname)_$(ij[1])$(ij[2])" => name_p)
            end
        else
            name_p = put_name!(p, names_dict, buf, lang) 
            push!(port_lst, "$(pname)" => name_p)
        end        
    end

    #next outputs
    blst = pmap.blst
    f! = t -> begin
        if t !== pmap
            n = length(names_dict) + 1
            name_t = get_name(t, n)
            push!(names_dict, t => name_t)
            push!(port_lst, "$(t.pname)" => name_t)
        else
            push!(port_lst, "$(t.pname)" => x)
        end
    end

    for p in blst
        if p isa SigVec || p isa SigMat
            for s in p
                f!(s)
            end
        else
            f!(p)
        end   
    end

    print_port_map_body(x, comp_name, arch_name, port_lst, pmap.lib, buf, lang)
end


function print_assignment!(p::SigConverter{S, N}, x, names_dict::Dict{AbstractSignal, String}, buf, lang) where {S, N}
    name = put_name!(p.from, names_dict, buf, lang)
    println(buf, INDENT_TOKEN, assign_op_str(x, lang), conv_func_str(p, name, lang), expr_terminator_str(lang))
end

function print_assignment!(p::Signal{S, N}, x, names_dict::Dict{AbstractSignal, String}, buf, lang) where {S, N}
    if !isnothing(p.from)
        name = put_name!(p.from, names_dict, buf, lang)
        #No type conversion needed here, since a Signal buffer is allowed only 
        #on other signals with same underlying type and bit length
        println(buf, INDENT_TOKEN, assign_op_str(x, lang), name, expr_terminator_str(lang))
    end
end

@inline print_slice(i, x, name, buf, lang) = println(buf, INDENT_TOKEN, assign_op_str(x, lang), range_str(name, i, lang), expr_terminator_str(lang))
@inline print_slice(is::AbstractVector, x, name, buf, lang) = println(buf, INDENT_TOKEN, assign_op_str(x, lang),
    concate_token_str((range_str(name, i, lang) for i in is), lang), expr_terminator_str(lang))
@inline print_slice(i::Pair, x, name, buf, lang) = println(buf, INDENT_TOKEN, assign_op_str(x, lang), range_str(name, i[1], i[2], lang), expr_terminator_str(lang))

function print_assignment!(p::SigSlice, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    name = put_name!(p.from, names_dict, buf, lang)
    if p.idx isa Isig
        name_i = put_name!(p.idx, names_dict, buf, lang)
        print_slice(name_i, x, name, buf, lang)
    else
        print_slice(p.idx, x, name, buf, lang)
    end
end

function print_assignment!(op::Concatenator, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    names_lst = []
    for p in op.from
        name = put_name!(p, names_dict, buf, lang)
        if op isa Concatenator{Logical}
            name = length(p) > 1 && !(p isa ConstSig) ? cast_bit_to_bit_vec(name, Logical, lang)  : name
        end
        push!(names_lst, name)
    end 

    println(buf, INDENT_TOKEN, assign_op_str(x, lang), concate_token_str(names_lst, lang), expr_terminator_str(lang))
end

function print_assignment!(op::AbstractOperator, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
    name_lst = []
    for p in op.from
        name = put_name!(p, names_dict, buf, lang)
        push!(name_lst, name)
    end

    s = operator_str(name_lst, op.operator, lang)

    if op isa BitwiseOperator && op.invert
        println(buf, INDENT_TOKEN, assign_op_str(x, lang), invert_op_str(s, lang), expr_terminator_str(lang))
    else
        println(buf, INDENT_TOKEN, assign_op_str(x, lang), s, expr_terminator_str(lang))
    end
end

function print_assignment!(op::ShiftOp, x, names_dict::Dict{AbstractSignal, String}, buf, lang)
   name = put_name!(op.from, names_dict, buf, lang)
#    println(buf, INDENT_TOKEN, "$(x) <= $(op.operator)(", name,", ", op.ncount, ");")
    println(buf, INDENT_TOKEN, assign_op_str(x, lang), shift_op_str(name, op.operator, op.ncount, lang), expr_terminator_str(lang))
end

function generate_architecture(component::T, io, lang) where T<:AbstractComponent{A} where A  
    #= The signal assignments should be printed after definitions, but
     because it is more efficient to print the assignments first and building
     the list of names at the same time, we'll just dump everything in a buffer,
     and print later to the actual file =#
    buf = IOBuffer()

    fields_names = fieldnames(T)
    fields = getfield.(Ref(component), fields_names)

    #First, the ports are added to the names dict
    names_dict = Dict{AbstractSignal, String}()
    for (port, field) in zip(fields, fields_names)
        #Strip field names of their direction substring
        name, dir = rsplit(String(field), "_"; limit=2)
        if port isa AbstractSignal
            if dir == "o" || dir == "io"
                port = Signal(port)
            end
            names_dict[port] = name
        elseif port isa SigVec
            for (i, p) in enumerate(port)
                if dir == "o" || dir == "io"
                    p = Signal(p)
                end
                name_p = "$(name)_$(i)"
                names_dict[p] = name_p
            end
        elseif port isa SigMat
            for ij in CartesianIndices(port)
                p = port[ij]
                if dir == "o" || dir == "io"
                    p = Signal(p)
                end
                name_p = "$(name)_$(ij[1])$(ij[2])"
                names_dict[p] = name_p
            end
        elseif isa(port, CompositeInterface)
            #TODO
        else
            error("Unsupported type specified for port.")
        end
    end

    #Next, we recursively print assignment statements for each node
    fields = collect(keys(names_dict))
    for port in fields
        print_assignment!(port, names_dict[port], names_dict, buf, lang)
    end

    print_arch_body(component, names_dict, fields, buf, io, lang)

    close(buf)
end

function print_port(buf, ports::T where T <: SigVec, islast, x, dir, lang)
    for (i, port) in enumerate(ports)
        print_port(buf, port, islast && (i == length(ports)), "$(x)_$(i)", dir, lang)
    end
end
function print_port(buf, ports::T where T <: SigMat, islast, x, dir, lang)
    for ij in CartesianIndices(ports)
        print_port(buf, ports[ij], islast && (ij[1]*ij[2] == length(ports)), "$(x)_$(ij[1])$(ij[2])", dir, lang)
    end
end

function generate_ports(component::T, io, lang::AbstractHDL) where T<:AbstractComponent{A} where A
    println(io, port_list_start(lang))
    #Declare port (inputs/outputs)
    fnames = fieldnames(T)
    for (i, field) in enumerate(fnames)
        port = getfield(component, field)
        namedir = rsplit(String(field), "_"; limit=2)
        if length(namedir) < 2
            error("Missing '_' delimiter between name and direction of port.")
        elseif occursin(namedir[2], "io")
            print_port(io, port, i == length(fnames), namedir..., lang)
        else
            error("Invalid direction :$(namedir[2]) specified for port \"$(namedir[1])\" in component. Must be 'i', 'o', or 'io' separated by '_'.")
        end        
    end
    println(io, port_list_end(lang))
end

function generate(component::T, io = stdout, output_lang::AbstractHDL=VHDL_Lang()) where T<:AbstractComponent{A} where A
    #Include std libs
    if !isa(A, Symbol)
        @error "Specified architecture name is not of Symbol type."
        return nothing
    end

    name = T.name.name
    print_header_pre(io, name, output_lang)  
    generate_ports(component, io, output_lang)
    print_header_post(io, name, output_lang)
    
    generate_architecture(component, io, output_lang)
end

export generate