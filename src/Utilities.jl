#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

@doc raw"""
    insert_select_blk! 
    
    Takes a `(a,b,...) = let ... (a,b,...) end` expression, generated from a `@select`
    macro, and does the following steps:
        1. replaces the the vals `a,b,...` with temporary names `a_$(case), b_$(case),...`
            in the `let` expression
        2. inserts `case => a_$(case), case => b_$(case),...` for each `a,b,...` entry
            in the expression dictionary
        3. escapes and inserts the modified `let` expression inside the `extra_expr`
            list, to be inserted above all the `Mux` statements later

    inputs:

        case - an expression/Symbol that represents a `Mux` case
        expr - the `let` expression
        expr_dict - dictionary of lists of pair expressions
        extra_expr - a list of extra expressions

    outputs:

        nothing

    modifies:

        expr_dict, extra_expr
"""
function insert_select_blk!(case, expr, expr_dict, extra_expr)
    @assert expr.head == :(=) && expr.args[1].head == :tuple && expr.args[2].head == :let
    
    for i in eachindex(expr.args[1].args)
        lhs = expr.args[1].args[i]
        lhs_case = Symbol("$(lhs)_$case")
        expr.args[1].args[i] = lhs_case

        if haskey(expr_dict, lhs)
            push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(lhs_case)))
        else
            expr_dict[lhs] = [ Expr(:call, esc(:(=>)), esc(case), esc(lhs_case)) ]
        end
    end
    push!(extra_expr, esc(expr))
end

@doc raw"""
    update_mux_dict! 
    
    Takes a `:block` expression, scans its arguments for `a = b` or `@select` macro expressions.
    For the assignment expressions `a = b`, the function updates an `expr_dict` dictionary with lists 
    of pair expressions like `case => b`. Alternatively if the input expression is a single `a = b` 
    expression, then it directly performs this operation. The lhs' of the assignment expressions
    are used as keys of the dict.

    Alternatively, if a `@select` macro call is encountered, then the macro is expanded in the current
    module, and the generated expression is modified using `insert_select_blk!`, and inserted into the `extra_expr` list,
    which will in turn be inserted above the `Mux` expressions later on. See @ref`insert_select_blk!` for
    more details.

    inputs:

        case - an expression/Symbol that represents a `Mux` case
        expr - the `:block` expression
        expr_dict - dictionary of lists of pair expressions
        extra_expr - a list of extra expressions

    outputs:
        
        nothing

    modifies:

        expr_dict, extra_expr
"""
function update_mux_dict!(case, expr, expr_dict, extra_expr)
    if expr.head == :block
        for e in expr.args
            if !(e isa LineNumberNode)
                if e.head == :(=)
                    lhs, rhs = e.args[1:2]
                    if haskey(expr_dict, lhs)
                        push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(rhs)))
                    else
                        expr_dict[lhs] = [ Expr(:call, esc(:(=>)), esc(case), esc(rhs)) ]
                    end
                elseif e.head == :macrocall && e.args[1] == Symbol("@select")
                    e2 = macroexpand(@__MODULE__, e)
                    insert_select_blk!(case, e2, expr_dict, extra_expr)
                end
            end
        end
    elseif expr.head == :(=)
        lhs, rhs = expr.args[1:2]
        if haskey(expr_dict, lhs)
            push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(rhs)))
        else
            expr_dict[lhs] = [ Expr(:call, esc(:(=>)), esc(case), esc(rhs)) ]
        end
    elseif expr.head == :macrocall && expr.args[1] == Symbol("@select")
        e2 = macroexpand(@__MODULE__, expr)
        insert_select_blk!(case, e2, expr_dict, extra_expr)
    end
end
macro select(sel, expr)
    expr_dict = Dict{Symbol, Any}()
    extra_expr = []

    #First, we construct a dictionary of muxes
    if expr.head == :block 
        for e in expr.args
            if !(e isa LineNumberNode) && e.head == :macrocall
                if e.args[1] == Symbol("@when")
                    update_mux_dict!(e.args[3], e.args[4], expr_dict, extra_expr)
                elseif e.args[1] == Symbol("@others")
                    update_mux_dict!(nothing, e.args[3], expr_dict, extra_expr)
                end
            end
        end
    end

    #Now that we have reparsed everything, we'll build a new expression
    e = Expr(:block)
    p = nothing
    push!(e.args, extra_expr...)
    etuple = Expr(:tuple)
    for (k, v) in expr_dict
        if p === nothing
            e2 = Expr(:call, esc(:Mux), esc(:nothing), esc(sel), v...)
            e2 = Expr(:(=), esc(k), e2 )
            p = k
        else
            e2 = Expr(:call, esc(:Mux), esc(p), esc(sel), v...)
            e2 = Expr(:(=), esc(k), e2 )
        end
        push!(e.args, e2)
        push!(etuple.args, esc(k))
    end
    
    push!(e.args, etuple)
    e = Expr(:let, Expr(:block), e)
    e = Expr(:(=), etuple, e)

    return e
end

@doc raw"""
    insert_select_blk! 
    
    This is the `Cond` version of the function.
    Takes a `(a,b,...) = let ... (a,b,...) end` expression, generated from a `@select`
    macro, and does the following steps:
        1. replaces the the vals `a,b,...` with temporary names `a_$(case), b_$(case),...`
            in the `let` expression
        2. inserts `case => a_$(case), case => b_$(case),...` for each `a,b,...` entry
            in the expression dictionary
        3. escapes and inserts the modified `let` expression inside the `extra_expr`
            list, to be inserted above all the `Cond` statements later

    inputs:

        case - an expression/Symbol that represents a `Cond` case 
        expr - the `let` expression
        expr_dict - dictionary of lists of pair expressions
        case_lst - list of cases so far
        extra_expr - a list of extra expressions

    outputs:

        nothing

    modifies:

        expr_dict, extra_expr
"""
function insert_select_blk!(case, expr, expr_dict, case_lst, extra_expr)
    @assert expr.head == :(=) && expr.args[1].head == :tuple && expr.args[2].head == :let
    
    for i in eachindex(expr.args[1].args)
        lhs = expr.args[1].args[i]
        lhs_case = Symbol("$(lhs)_$case")
        expr.args[1].args[i] = lhs_case

        if haskey(expr_dict, lhs)
            push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(lhs_case)))
        else #new conditional
            expr_dict[lhs] = vcat((Expr(:call, esc(:(=>)), esc(case2), esc(:nothing)) for case2 in case_lst)..., #stuff nothings
                            Expr(:call, esc(:(=>)), esc(case), esc(lhs_case)) ) #then add actual statement
        end
    end
    push!(extra_expr, esc(expr))
end
function update_cond_lst!(case, expr, expr_dict, case_lst, extra_expr)
    ncase = length(case_lst)
    if case isa Expr && case.head === :call
        if case.args[1] == :(==)
            case.args[1] = :eq
        elseif case.args[1] == :(!=)
            case.args[1] = :neq
        end
    end

    if expr.head === :block
        for e in expr.args
            if !(e isa LineNumberNode) 
                if e.head == :(=)
                    lhs, rhs = e.args[1:2]
                    if haskey(expr_dict, lhs)
                        push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(rhs)))
                    else #new conditional
                        expr_dict[lhs] = vcat((Expr(:call, esc(:(=>)), esc(case2), esc(:nothing)) for case2 in case_lst)..., #stuff nothings
                                        Expr(:call, esc(:(=>)), esc(case), esc(rhs)) ) #then add actual statement
                    end
                elseif e.head == :macrocall && e.args[1] == Symbol("@select")
                    e2 = macroexpand(@__MODULE__, e)
                    insert_select_blk!(case, e2, expr_dict, case_lst, extra_expr)
                end
            end
        end
    elseif expr.head == :(=)
        lhs, rhs = expr.args[1:2]
        if haskey(expr_dict, lhs)
            push!(expr_dict[lhs], Expr(:call, esc(:(=>)), esc(case), esc(rhs)))
        else #new conditional
            expr_dict[lhs] = vcat((Expr(:call, esc(:(=>)), esc(case2), esc(:nothing)) for case2 in case_lst)..., #stuff nothings
                            Expr(:call, esc(:(=>)), esc(case), esc(rhs)) ) #then add actual statement
        end
    elseif expr.head == :macrocall && expr.args[1] == Symbol("@select")
        e2 = macroexpand(@__MODULE__, expr)
        insert_select_blk!(case, e2, expr_dict, case_lst, extra_expr)
    end
    #Some conditionals are not covered in this case, so we need to fill with nothings
    ncase = length(case_lst)
    for v in values(expr_dict)
        if length(v) == ncase
            push!(v, Expr(:call, esc(:(=>)), esc(case), esc(:nothing)) )
        end
    end
    push!(case_lst, case)
end
macro select(expr)
    expr_dict = Dict{Symbol, Any}()
    case_lst = []
    extra_expr = []

    #First, we construct a dictionary of conditionals
    if expr.head == :block 
        for e in expr.args
            if !(e isa LineNumberNode) && e.head == :macrocall
                if e.args[1] == Symbol("@when")
                    update_cond_lst!(e.args[3], e.args[4], expr_dict, case_lst, extra_expr)
                elseif e.args[1] == Symbol("@others")
                    update_cond_lst!(nothing, e.args[3], expr_dict, case_lst, extra_expr)
                end
            end
        end
    else
        error("expected block statement.")
    end

    #Now that we have reparsed everything, we'll build a new expression
    e = Expr(:block)
    p = nothing
    push!(e.args, extra_expr...)
    etuple = Expr(:tuple)
    for (k, v) in expr_dict
        if p === nothing
            e2 = Expr(:call, esc(:Cond), esc(:nothing), v...)
            e2 = Expr(:(=), esc(k), e2 )
            p = k
        else
            e2 = Expr(:call, esc(:Cond), esc(p), v...)
            e2 = Expr(:(=), esc(k), e2 )
        end
        push!(e.args, e2)
        push!(etuple.args, esc(k))
    end
    push!(e.args, etuple)
    e = Expr(:let, Expr(:block), e)
    e = Expr(:(=), etuple, e)
    return e
end
macro when(case, expr) end #Dummy macro
macro others(expr) end

macro slices(expr)
    if expr.head === :ref
        a = expr.args[1]
        args = Expr(:tuple, expr.args[2:end]...)
    end

    return :( (($(esc(a))[i] for i in $(esc(args)))...,) )
end

macro uc_str(str)
    return :( $(esc(uc(str)))  )
end
macro sc_str(str)
    return :( $(esc(sc(str)))  )
end
macro lc_str(str)
    return :( $(esc(lc(str)))  )
end

export @select, @when, @others, @slices, @uc_str, @sc_str, @lc_str

function generate_file(comp::T; outputdir=".", outfile="") where T<:AbstractComponent{A} where A
    if !isdir(outputdir)
        mkpath(outputdir)
    end
    fname = outfile === "" ? joinpath(outputdir,"$(T.name.name)_$A.vhd") : joinpath(outputdir, outfile)
    open(fname, "w") do file
        generate(comp, file)
    end
end
export generate_file

function Base.show(io::IO, p::AbstractSignal{S, N}, lang=VHDL_Lang()) where {S, N}
    print(io, typeof(p), "; HDL: signal ", prim_type_str(p, lang))
end

function dump_hdl(ps::AbstractSignal...;io::IO=stdout, showsigs=true, lang=VHDL_Lang())
    buf = IOBuffer(write=true)
    names_dict = Dict{AbstractSignal, String}()
    for p in ps
        JuliaHDL.put_name!(p, names_dict, buf, lang)
    end

    if showsigs
        #Print signal definitions first
        println(io, "============\t Signals\t ============")
        for (signal, name) in names_dict
            println(io, INDENT_TOKEN, "signal ", name, " : ", prim_type_str(signal, lang), ";")
        end
    end
    
    s = read(seekstart(buf), String)
    if s !== ""
        if showsigs 
            println(io, "============\t Assignments\t ============")
        end
        print(io, s)
    end
    return nothing
end
function dump_hdl(ps::NTuple{L, AbstractSignal} where L, lang=VHDL_Lang();io::IO=stdout)
    Base.show(io, ps...)
end
export dump_hdl

