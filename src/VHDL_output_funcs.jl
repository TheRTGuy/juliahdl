struct VHDL_Lang <: AbstractHDL
    standard::String
    libs::Vector{String}
end

VHDL_Lang(standard="", libs=String[]) = VHDL_Lang(standard, libs)
export VHDL_Lang

@inline function print_header_pre(io, name, lang::VHDL_Lang)
    println(io, "library IEEE;")
    println(io, "use IEEE.STD_LOGIC_1164.ALL;")
    println(io, "use IEEE.numeric_std.all;")
    for lib in lang.libs
        println(io, "use ", lib)
    end
    println(io)  
    println(io, "entity ", name, " is")
end

@inline function print_header_post(io, name, ::VHDL_Lang)
    println(io, "end $(name);")
end

@inline port_list_start(::VHDL_Lang) = "port ("
@inline port_list_end(::VHDL_Lang) = ");"

const TypeStr_VHDL = Dict(Unsigned => "unsigned",
                     Signed => "signed",
                     Logical => "std_logic_vector",
                     Integer => "integer",
                     Bool => "boolean")

@inline type_str_token(T::Type) = TypeStr_VHDL[T]
@inline prim_type_str(::T where T<:AbstractSignal{Logical, 1}, ::VHDL_Lang) = "std_logic"  
@inline prim_type_str(::T where T<:AbstractSignal{Bool, 1}, ::VHDL_Lang) = "boolean"  
@inline prim_type_str(::AbstractSignal{S, N}, ::VHDL_Lang) where {S, N} = TypeStr_VHDL[S] * "($(N-1) downto 0)"
@inline function prim_type_str(::Isig{P}, ::VHDL_Lang) where P
    if P.stop == typemax(Int32) && P.start == typemin(Int32)
        return "integer"
    elseif 0 ≤ P.start
        return "natural range $(P.start) to $(P.stop)"
    else
        return "integer range $(P.start) to $(P.stop)"
    end
end

const DIR_DICT_VHDL = Dict("i" => "in", "o" => "out", "io" => "inout")
@inline print_port(buf, port::AbstractSignal, islast, x, dir, lang::VHDL_Lang) = println(buf, INDENT_TOKEN, x, " : $(DIR_DICT_VHDL[dir]) ", prim_type_str(port, lang), islast ? "" : ";")

@inline cast_bit_to_bit_vec(name, S, ::VHDL_Lang) = TypeStr_VHDL[S] * "(" * name * ")"

@inline function conv_func_str(p::AbstractSignal{S, N}, name, ::VHDL_Lang) where {S<:BitsSigType, N}
    if p.from isa Isig
        return "to_" * TypeStr_VHDL[S] * "(" * name * ", $N)"
    elseif !(p.from isa AbstractSignal{S})
        return TypeStr_VHDL[S] * "(" * name * ")"
    else
        return name
    end
end
@inline function conv_func_str(p::Isig{P}, name, ::VHDL_Lang) where P
    if p.from isa Union{Usig, Ssig}
        return "to_" * TypeStr_VHDL[Integer] * "(" * name * ")"
    else
        return name
    end
end
@inline function conv_func_str(p::Wire, name, ::VHDL_Lang)
    if p.from isa Boolean
        return "'1' when " * name * " else '0'"
    else
        name
    end    
end
@inline function conv_func_str(p::Boolean, name, ::VHDL_Lang)
    if p.from isa Wire
        return name * " = '1'"
    else
        name
    end    
end

@inline expr_terminator_str(::VHDL_Lang) = ";"
@inline assign_op_str(x, ::VHDL_Lang) = "$(x) <= "

function print_reg_body(x, rising_edge, clk_name, rst_name, blst_str, blst_init_str, blst_from_str, slst_str, buf, ::VHDL_Lang)
    println(buf, INDENT_TOKEN, "$(x)_proc : process(", join(slst_str, ", "), ")")
    println(buf, INDENT_TOKEN, "begin")
    edge = rising_edge ? "rising_edge" : "falling_edge"
    if isnothing(rst_name)
        println(buf, INDENT_TOKEN^2, "if ", edge, "(", clk_name, ") then")
    else
        println(buf, INDENT_TOKEN^2, "if ", rst_name, " = '0' then")
        for (reg_sig_name, init_sig_name) in zip(blst_str, blst_init_str)
            println(buf, INDENT_TOKEN^3, reg_sig_name, " <= ", init_sig_name, ";")
        end
        println(buf, INDENT_TOKEN^2, "elsif ", edge, "(", clk_name, ") then")
    end
    for (reg_sig_name, from_sig_name) in zip(blst_str, blst_from_str)
        println(buf, INDENT_TOKEN^3, reg_sig_name, " <= ", from_sig_name, ";")
    end
    println(buf, INDENT_TOKEN^2, "end if;")
    
    println(buf, INDENT_TOKEN, "end process $(x)_proc;")
end

function print_inline_mux_body(x, sel_name, names_lst, cases_lst, buf, ::VHDL_Lang)
    println(buf, INDENT_TOKEN, "with ", sel_name, " select ", x, " <=")
    for (name, case) in zip(@view(names_lst[1:end-1]), @view(cases_lst[1:end-1]))
        println(buf, INDENT_TOKEN^2, name, " when ", case, ",")
    end
    end_case = cases_lst[end] === nothing ? "others" : cases_lst[end]

    println(buf, INDENT_TOKEN^2, names_lst[end], " when ", end_case, ";")
end

function print_mux_body(x, sel_name, slst_str, case_dict, buf, ::VHDL_Lang)
    #Print body of process
    println(buf, INDENT_TOKEN, "$(x)_proc : process(", join(slst_str, ", "), ")")
    println(buf, INDENT_TOKEN, "begin")
    println(buf, INDENT_TOKEN^2, "case ", sel_name, " is")
    for (k, v) in case_dict
        if k !== nothing
            println(buf, INDENT_TOKEN^2, "when ", k, " =>")
            for (dst, src) in v
                println(buf, INDENT_TOKEN^3, dst, " <= ", src, ";")
            end
        end
    end
    println(buf, INDENT_TOKEN^2, "when others =>") #we must always have this
    if nothing in keys(case_dict) #however, we don't have to actually assign anything
        for (dst, src) in case_dict[nothing]
            println(buf, INDENT_TOKEN^3, dst, " <= ", src, ";")
        end
    end
    println(buf, INDENT_TOKEN^2, "end case;")
    
    println(buf, INDENT_TOKEN, "end process $(x)_proc;")
end

function print_inline_cond_body(x, src_lst, cond_lst, buf, ::VHDL_Lang)
    println(buf, INDENT_TOKEN, x, " <=")
    for (src, cond) in zip(@view(src_lst[1:end-1]), cond_lst)
        println(buf, INDENT_TOKEN^2, src, " when ", cond, " else")
    end
    println(buf, INDENT_TOKEN^2, src_lst[end], ";")
end

function print_cond_body(x, slst_str, cond_src_lst, buf, ::VHDL_Lang)
    println(buf, INDENT_TOKEN, "$(x)_proc : process(", join(slst_str, ", "), ")")
    println(buf, INDENT_TOKEN, "begin")
    println(buf, INDENT_TOKEN^2, "if ", cond_src_lst[1][1], " then")
    for (src, dst) in cond_src_lst[1][2]
        println(buf, INDENT_TOKEN^3, dst, " <= ", src, ";")
    end
    for p in @view(cond_src_lst[2:end])
        if p[1] !== nothing
            println(buf, INDENT_TOKEN^2, "elsif ", p[1], " then")
        else
            println(buf, INDENT_TOKEN^2, "else")
        end
        for (src, dst) in p[2]
            println(buf, INDENT_TOKEN^3, dst, " <= ", src, ";")
        end
    end
    println(buf, INDENT_TOKEN^2, "end if;")
    println(buf, INDENT_TOKEN, "end process $(x)_proc;")
end

function print_port_map_body(x, comp_name, arch_name, port_lst, lib, buf, ::VHDL_Lang)
    println(buf, INDENT_TOKEN, comp_name, "_", arch_name, "_", x, " : entity ", 
        lib == "" ? "" : lib * ".", comp_name*"(" * arch_name * ")")
    println(buf, INDENT_TOKEN, "port map(")
    for (src, dst) in @view(port_lst[1:end-1])
        println(buf, INDENT_TOKEN^2, src, " => ", dst, ",")
    end
    println(buf, INDENT_TOKEN^2, port_lst[end][1], " => ", port_lst[end][2], ");")
end

@inline range_str(name, i1, i2, ::VHDL_Lang) = name * (i1 > i2 ? "($i1 downto $i2)" : "($i1 to $i2)")
@inline range_str(name, i, ::VHDL_Lang) = name * "($i)"
@inline concate_token_str(names, ::VHDL_Lang) = join(names, " & ")

@inline get_const_val_str(s::ConstSig{S} where S<:BitsSigType, ::VHDL_Lang) = #= all(c == s.val[1] for c in s.val) ? "(others => '$(s.val[1])')" : =# "\""*s.val*"\""
@inline get_const_val_str(s::ConstSig{Logical, 1}, ::VHDL_Lang) = "'"*s.val*"'"
@inline get_const_val_str(s::Union{ic, bc}, ::VHDL_Lang) = "$(s.val)"

@inline operator_str(names_lst, op, ::VHDL_Lang) = length(names_lst) == 1 ? (" $op " * names_lst[1]) : join(names_lst, " $op ")
@inline invert_op_str(s, ::VHDL_Lang) = "not(" * s * ")"
@inline shift_op_str(name, op, n, ::VHDL_Lang) = "$op($name, $n)"

function print_arch_body(::T, names_dict, fields, buf, io, lang::VHDL_Lang) where T<:AbstractComponent{A} where A
    name = T.name.name
    println(io, "architecture ", A, " of $(name) is")

    #Print signal definitions first
    for (signal, name) in names_dict
        if !(signal in fields)
            println(io, INDENT_TOKEN, "signal ", name, " : ", prim_type_str(signal, lang), ";")
        end
    end

    println(io, "begin")
    print(io, read(seekstart(buf), String))
    println(io, "end ", A, ";")
end