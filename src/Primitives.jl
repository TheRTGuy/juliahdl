#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

global suppress_warn = true
const suppress_warn_msg = "(to suppress this and other warnings, set `$(@__MODULE__).suppress_warn = true` !"

abstract type Logical <: Integer end

@doc """
    AbstractSignal{S <: Integer, N} <: AbstractInterface

Supertype for all primitive objects used in `JuliaHDL`
"""
abstract type AbstractSignal{S <: Integer, N} <: AbstractInterface end
BitsSigType = Union{Unsigned, Signed, Logical}
BitsSignal{N} = AbstractSignal{S, N} where S <: BitsSigType
TNothing{T} = Union{Nothing, T}

AbstractNSig{N} = AbstractSignal{S, N} where S
export AbstractSignal, AbstractNSig, Logical
#Aliases
for (p1, p2) in [(:Usig, :Unsigned), 
    (:Ssig, :Signed), 
    (:Lsig, :Logical),
    (:Isig, :Integer)]
    @eval begin 
        @doc """
            $($p1){N}

        Alias for `AbstractSignal{$($p2), N}`
        """
        $p1{N} = AbstractSignal{$p2, N}
    end
end

@doc """
    Wire

Alias for `AbstractSignal{Logical, 1}`
"""
Wire = AbstractSignal{Logical, 1}
@doc """
    Boolean

Alias for `AbstractSignal{Bool, 1}`
"""
Boolean = AbstractSignal{Bool, 1}

@doc """
    SigVec{S, N, L} <: AbstractVector{AbstractSignal{S, N}} 

One-dimensional static array of `L` primitive signals of underlying type `S` and bit length `N`.
"""
struct SigVec{S, N, L} <: AbstractVector{AbstractSignal{S, N}} 
    prims::NTuple{L, AbstractSignal{S, N}}
end

@doc """
    SigVec(ps::AbstractVector{<:AbstractSignal{S, N}}) where {S, N}

Construct a `SigVec` from a `AbstractVector` of `AbstractSignal{S, N}`

# Examples
```julia-repl
julia> x = SigVec([Usig(5), Usig(5)])
2-element UsigVec{5, 2}:
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
```
"""
function SigVec(ps::AbstractVector{<:AbstractSignal{S, N}}) where {S, N}
    SigVec((ps...,))
end

@doc """
    SigVec(ps::Base.Generator)

Construct a `SigVec` from a generator object.

# Examples
```julia-repl
julia> x = SigVec(Usig(5) for _ = 1:2)
2-element UsigVec{5, 2}:
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
```
"""
function SigVec(ps::Base.Generator)
    SigVec(collect(ps))
end

@doc """
    SigVec(ps::AbstractSignal{S, N}...) where {S, N}

Construct a `SigVec` from a variable list of `AbstractSignal{S, N}` arguments

# Examples
```julia-repl
julia> x = SigVec(Usig(5), Usig(5))
2-element UsigVec{5, 2}:
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
```
"""
function SigVec(ps::AbstractSignal{S, N}...) where {S, N}
    SigVec((ps...,))
end
Base.size(::SigVec{S, N, L} where {S, N}) where L = (L,)
Base.IndexStyle(::Type{<:SigVec}) = IndexLinear()
Base.getindex(vec::SigVec, i::Int) = vec.prims[i]

for (T, S) in ( (:UsigVec, :Unsigned), (:SsigVec, :Signed), (:LsigVec, :Logical), (:IsigVec, :Integer) )
    @eval begin
        @doc """
            $($T){N, L}

        Alias for `SigVec{$($S), N, L}`
        """
        $T{N, L} = SigVec{$S, N, L}
    end
end
@doc """
    WireVec{N, L}

Alias for `SigVec{Logical, N, L}`
"""
WireVec{L} = SigVec{Logical, 1, L}

@doc """
    BoolVec{N, L}

Alias for `SigVec{Bool, N, L}`
"""
BoolVec{L} = SigVec{Bool, 1, L}

export SigVec, UsigVec, SsigVec, LsigVec, IsigVec, WireVec, BoolVec

@doc """
    SigMat{S, N, L, K} <: AbstractMatrix{AbstractSignal{S, N}} 

Two-dimensional static `L × K` array of primitive signals of underlying type `S` and bit length `N`.
"""
struct SigMat{S, N, L, K} <: AbstractMatrix{AbstractSignal{S, N}}
    sigs::NTuple{M, AbstractSignal{S, N}} where M
    @doc """
        SigMat(sigs::NTuple{M, AbstractSignal{S, N}}, L::T, K::T) where {S, N, M, T<:Integer}
    
    Construct an `SigMat` from a tuple of `AbstractSignal` with size `L × K`.

    # Examples
    ```julia-repl
    julia> A = SigMat( (Usig(5), Usig(5), Usig(5), Usig(5)), 2, 2)
    2×2 UsigMat{5, 2, 2}:
    Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
    Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
    ```
    """
    function SigMat(sigs::NTuple{M, AbstractSignal{S, N}}, L::T, K::T) where {S, N, M, T<:Integer}
        if L*K != M
            ArgumentError("Dimension mismatch with tuple length $L x $K != $M !") |> throw
        end
        new{S, N, L, K}(sigs)
    end
end

@doc """
    SigMat(ps::AbstractMatrix{<:AbstractSignal{S, N}}) where {S, N}

Construct an `SigMat` from a matrix of `AbstractSignal`.

# Examples
```julia-repl
julia> A = SigMat( [Usig(5) Usig(5); Usig(5) Usig(5)] )
2×2 UsigMat{5, 2, 2}:
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
```
"""
function SigMat(ps::AbstractMatrix{<:AbstractSignal{S, N}}) where {S, N}
    SigMat(tuple(ps...), size(ps)...)
end

@doc """
    SigMat(ps::Base.Generator)

Construct an `SigMat` from a generator object.

# Examples
```julia-repl
julia> A = SigMat( Usig(5) for _ = 1:2, _ = 1:2 )
2×2 UsigMat{5, 2, 2}:
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
 Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)  Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
```
"""
function SigMat(ps::Base.Generator)
    SigMat(collect(ps))
end
Base.size(::SigMat{S, N, L, K} where {S, N}) where {L, K} = (L, K)
Base.IndexStyle(::Type{<:SigMat}) = IndexLinear()
Base.getindex(mat::SigMat, i::Int) = mat.sigs[i]

for (T, S) in ( (:UsigMat, :Unsigned), (:SsigMat, :Signed), (:LsigMat, :Logical) )
    @eval begin
        @doc """
            $($T){N, L, K}

        Alias for `SigMat{$($S), N, L, K}`
        """
        $T{N, L, K} = SigMat{$S, N, L, K}
    end
end
export SigMat, UsigMat, SsigMat, LsigMat

const AllowedSigVals = ('U','X','1','0','Z','W','H','L','-')
check_allowed_sigval(val) = all(uppercase(c) in AllowedSigVals for c in val)

@doc """
    Signal{S, N} <: AbstractSignal{S, N}

A mutable primitive objects that is typically used for inputs and outputs.
"""
mutable struct Signal{S, N} <: AbstractSignal{S, N}
    init::Union{String, Integer}
    from::Union{AbstractSignal{S, N}, Nothing}

    Signal{S}(n::Integer) where S<:BitsSigType = new{S,n}("0"^n, nothing)
    function Signal{S}(init::String) where S<:BitsSigType
        if !check_allowed_sigval(init)
            ArgumentError("initializer contains illegal characters!\n allowed are: $AllowedSigVals") |> throw
        end
        new{S, length(init)}(init, nothing)
    end
    function Signal{Integer, P}(init::Integer=0) where P 
        if !(P isa UnitRange{T} where T<:Integer)
            ArgumentError("range must be a unit range.") |> throw
        elseif !(init in P)
            ArgumentError("initial value outside of range.") |> throw
        end
        new{Integer, P}(init, nothing)
    end
    Signal{Bool}(b::Bool=false) = new{Bool, 1}(b, nothing)
    #These are "buffer" functions, and in principle are useless for the user
    #they are mainly useful for buffering output signals, such as registers in a loop
    Signal(p::AbstractSignal{S, N}) where {S<:BitsSigType, N} = new{S, N}("-"^N, p)
    Signal(i::AbstractSignal{Integer, P}) where P = new{Integer, P}(0, i)
    Signal(i::AbstractSignal{Bool, 1}) = new{Bool, 1}(false, i)
end

@doc """
    SigConverter{S, N} <: AbstractSignal{S, N}

A signal converter (type-cast) primitive 

# Examples

```juliarepl
julia> a = Usig(5)
Signal{Unsigned, 5}; HDL: signal unsigned(4 downto 0)
julia> b = JuliaHDL.SigConverter{Signed}(a)
JuliaHDL.SigConverter{Signed, 5}; HDL: signal signed(4 downto 0)
```
"""
struct SigConverter{S, N} <: AbstractSignal{S, N}
    from::AbstractSignal
    #Convert bits signals of same length
    SigConverter{S}(p::BitsSignal{N}) where {S<:BitsSigType, N} = new{S, N}(p) 
    #Convert integer to bit signal
    SigConverter{S, N}(i::AbstractSignal{Integer}) where {S<:Union{Signed, Unsigned}, N} = new{S, N}(i)
    #Convert numerical bit signal to integer
    function SigConverter{Integer, P}(p::AbstractSignal{S}) where {S<:Union{Signed, Unsigned}, P} 
        if !(P isa UnitRange{T} where T<:Integer && P.start ≤ P.stop)
            ArgumentError("range must be an increasing unit range.") |> throw
        end
        new{Integer, P}(p)
    end
    #Convert integer to another with different range
    function SigConverter{Integer, P}(i::AbstractSignal{Integer, R}) where {P, R}
        if !(P isa UnitRange{T} where T<:Integer && P.start ≤ P.stop)
            ArgumentError("range must be an increasing unit range.") |> throw
        end
        if !(P.start ≤ R.start && R.stop ≤ P.stop) && !suppress_warn
            @warn "The range $R of converted integer is not contained in the new range $P;\n"*
                  "Possible loss of information expected. " * suppress_warn_msg
        end
        new{Integer, P}(i)
    end
    #Boolean <-> Wire
    SigConverter{Bool, 1}(w::Wire) = new{Bool, 1}(w)
    SigConverter{Logical, 1}(b::Boolean) = new{Logical, 1}(b)
end
SigConverter{Integer}(p::AbstractSignal{S}) where S<:Union{Signed, Unsigned} = 
        SigConverter{Integer, Int64(typemin(Int32)):Int64(typemax(Int32))}(p)

import Base:length, range

Base.length(_::AbstractSignal{S, N} where S) where N = N
Base.range(_::Isig{P}) where P = P
Base.length(_::Isig{P}) where P = length(P)
export Signal

#Converters
AbstractSignal{S}(p::AbstractSignal) where S = SigConverter{S}(p)
AbstractSignal{S, N}(p::AbstractSignal) where {S, N} = SigConverter{S, N}(p) 
AbstractSignal{Logical, 1}(p::Union{Wire, Boolean}) = SigConverter{Logical, 1}(p)
AbstractSignal{Bool, 1}(p::Union{Wire, Boolean}) = SigConverter{Bool, 1}(p)

#Constructors
AbstractSignal{S}(n::Integer) where S <: BitsSigType = Signal{S}(n)
AbstractSignal{S}(init::String) where S <: BitsSigType = Signal{S}(init)
AbstractSignal{Logical, 1}() = Signal{Logical}(1)
AbstractSignal{Integer}(i::Integer=0; range=Int64(typemin(Int32)):Int64(typemax(Int32))) = 
                        Signal{Integer, range}(i)
AbstractSignal{Integer, P}(i::Integer=0) where P = Signal{Integer, P}(i)
AbstractSignal{Bool, 1}(b::Bool=false) = Signal{Bool}(b)                   
export Lsig, Usig, Ssig, Isig, Wire, Boolean

struct SigSlice{S<:BitsSigType, N} <: AbstractSignal{S, N}
    from::AbstractSignal{T, K} where {T, K}
    idx::Union{AbstractVector{I}, Pair{I, I}, I, Isig} where I <: Integer

    function SigSlice(p::AbstractSignal{T, K}, 
        i::I where I <: Integer) where {T, K}
        if !(0 ≤ i < K)
            ArgumentError("index not in range.") |> throw
        end
        return new{Logical, 1}(p, i)
    end
    function SigSlice(p::AbstractSignal{T, K}, 
        rng::Pair{I, I} where I <: Integer) where {T, K}
        if !(0 ≤ rng[1] < K && 0 ≤ rng[2] < K)
            ArgumentError("index not in range.") |> throw
        end
        len = (rng[2] > rng[1] ? rng[2] - rng[1] : rng[1] - rng[2]) + 1
        if len == 1
            return new{Logical, 1}(p, rng[1])
        end
        return new{T, len}(p, rng)
    end

    function SigSlice(p::AbstractSignal{T, K},
        arr::AbstractVector{I} where I <: Integer) where {T, K}
        if !all( 0 ≤ a < K for a in arr)
            ArgumentError("index not in range.") |> throw
        end
        len = length(arr)
        if len == 1
            return new{Logical, 1}(p, arr)
        end
        return new{Logical, len}(p, arr)
    end

    function SigSlice(p::AbstractSignal{T, K},
                        isig::Isig{P}) where {T, K, P}
        if !(0 ≤ P.start ≤ P.stop < K) && !suppress_warn #assume the user knows what they are doing
            @warn "The range $P of index integer is not contained in the range $(0:K-1);\n"*
                  "Possible runtime exception expected. " * suppress_warn_msg
        end
        return new{Logical, 1}(p, isig)
    end
end
export SigSlice
function Base.getindex(p::BitsSignal, rng::UnitRange{<:Integer})
    return SigSlice(p, rng.start => rng.stop)
end
function Base.getindex(p::BitsSignal,
    rng::Union{AbstractArray{I, 1}, Pair{I, I}, I, Isig} where I <: Integer)
    return SigSlice(p, rng)
end
function Base.getindex(p::BitsSignal{N}, ::Colon) where N
    return SigVec(SigSlice(p, i) for i = 0:N-1)
end
import Base:firstindex,lastindex
function Base.firstindex(p::BitsSignal{N}) where N
    return 0 
end
function Base.lastindex(p::BitsSignal{N}) where N
    return N-1  
end

struct ConstSig{S, N} <: AbstractSignal{S, N}
    val::Union{String, Integer}
    function ConstSig{S}(val::String) where S<:BitsSigType
        if !check_allowed_sigval(val)
            ArgumentError("constant contains illegal characters!\n allowed are: $AllowedSigVals") |> throw
        end
        new{S, length(val)}(val)
    end
    ConstSig{Integer}(i::Integer=0) = new{Integer, Int64(typemin(Int32)):Int64(typemax(Int32))}(i)
    function ConstSig{Integer, P}(i::Integer=P.start) where P 
        if !(P isa UnitRange{T} where T<:Integer)
            ArgumentError("range must be a unit range.") |> throw
        elseif !(P.start ≤ i ≤ P.stop)
            ArgumentError("constant value outside of range.") |> throw
        end
        new{Integer, P}(i)
    end
    ConstSig{Bool, 1}(b::Bool) = new{Bool, 1}(b)
end
ConstSig{S, N}() where {S<:BitsSigType, N} = ConstSig{S}("0"^N) 
function ConstSig{S, N}(v::T where T<:Integer) where {S <: Union{Signed, Unsigned}, N}
    ConstSig{S}(last(string(convert(S, v), base=2, pad=N), N))
end

export ConstSig
lc{N} = ConstSig{Logical, N}
uc{N} = ConstSig{Unsigned, N}
sc{N} = ConstSig{Signed, N}
ic{P} = ConstSig{Integer, P}
bc = ConstSig{Bool, 1}
export lc, uc, sc, ic, bc


import Base:^,*
^(s::String, ::Type{ConstSig{S, N} where N}) where S = ConstSig{S}(s)
function ^(s::String, ::AbstractSignal{S, N}) where {S, N}
    if length(s) !== N
        ArgumentError("constant does not have same bit count as signal!") |> throw
    end
    return ConstSig{S}(s)
end
function ^(i::Integer, ::AbstractSignal{S, N}) where {S, N}
    return ConstSig{S, N}(i)
end
*(i::Integer, ::Type{ConstSig{S, N}}) where {S, N} = ConstSig{S, N}(i)
*(i::Integer, ::Type{ConstSig{S}}) where S = ConstSig{S}(i)

abstract type AbstractOperator{S, N} <: AbstractSignal{S, N} end

struct Concatenator{S <: BitsSigType, N} <: AbstractOperator{S, N}
    from::NTuple{L, BitsSignal} where L
    function Concatenator(a::BitsSignal, 
                          b::BitsSignal, 
                          ps::BitsSignal...)
        
        len = length(a) + length(b) + (length(ps) > 0 ? sum( length(p) for p in ps ) : 0)                          
        P, _ = typeof(a).parameters
        if all(p isa AbstractSignal{P} for p in ps) &&
                b isa AbstractSignal{P}
            return new{P, len}((a, b, ps...))
        else
            return new{Logical, len}((a, b, ps...))
        end
    end
end
export Concatenator
^(ps::BitsSignal...) = Concatenator(ps...)

ArithSig{S, N} = Union{AbstractSignal{S, N}, Isig} where {S, N} #Special type used in arithmetic operators
struct ArithmeticOperator{S, N} <: AbstractOperator{S, N}
    operator::Symbol
    from::NTuple{L, ArithSig{S, N}} where L
end
# ArithmeticOperator(op::Symbol, from::ArithSig{S, N}...) where {S, N} = ArithmeticOperator(op, from)

import Base:+, -, /
# Arithmetic on unsigned/signed types
for (op, sym) in [(:+, :(:+)), (:*, :(:*)), (:/, :(:/))]
    @eval function $op(a::AbstractSignal{S, N}, 
                       b::AbstractSignal{S, N}, 
                       as::AbstractSignal{S, N}...)  where {S <: Union{Signed, Unsigned}, N}
        return ArithmeticOperator($sym, tuple(a, b, as...))
    end
    @eval @inline function $op(a::Isig, b::Isig, as::Isig...)
        l = minimum(range(p).start for p in (a, b, as...))
        u = maximum(range(p).stop for p in (a, b, as...))
        return ArithmeticOperator{Integer, l:u}($sym, tuple(a, b, as...))
    end
    @eval $op(a::ArithSig, ::Nothing) = a
    @eval $op(::Nothing, b::ArithSig) = b
    @eval $op(a::ArithSig{S,N}, b::Integer) where {S, N} = 
        $op(a, ConstSig{S, N}(b))
    @eval $op(a::Integer, b::ArithSig{S,N}) where {S, N} = 
        $op(ConstSig{S, N}(a), b)
end

function *(A::SigMat{S, N, L, K}, x::SigVec{S, N, K}) where {S, N, L, K}
    return SigVec(a'x for a in eachrow(A))
end

# - can be a unary operator
-(a::ArithSig) = ArithmeticOperator(:-, (a,))
function -(a::AbstractSignal{S, N}, b::AbstractSignal{S,N})   where {S <: Union{Signed, Unsigned}, N}
    return ArithmeticOperator(:-, (a, b))
end
@inline function -(a::Isig, b::Isig)
    l = min(range(a).start, range(b).start)
    u = max(range(a).stop, range(b).stop)
    return ArithmeticOperator{Integer, l:u}(:-, (a, b))
end
-(a::ArithSig, ::Nothing) = a
-(::Nothing, b::ArithSig) = b
-(a::ArithSig{S,N}, b::Integer) where {S, N} = 
    -(a, ConstSig{S, N}(b))
-(a::Integer, b::ArithSig{S,N}) where {S, N} = 
    -(ConstSig{S, N}(a), b)


struct BitwiseOperator{S<:Union{BitsSigType, Bool}, N} <: AbstractOperator{S, N}
    invert::Bool
    operator::Symbol
    from::NTuple{L, AbstractSignal{S, N}} where L
end

# Boolean algebra
import Base: &, |, xor
for (op, sym) in [(:&, :(:and)), (:|, :(:or)), (:(Base.xor), :(:xor)), 
                  (:nand, :(:nand)), (:nor, :(:nor)), (:xnor, :(:xnor))]
    @eval function $op(a::AbstractSignal{S, N}, 
                        b::AbstractSignal{S, N}, 
                        as::AbstractSignal{S, N}...)  where {S<:Union{BitsSigType, Bool}, N}
        return BitwiseOperator{S, N}(false, $sym, tuple(a, b, as...))
    end
    @eval $op(a::BitsSignal, ::Nothing) = a
    @eval $op(::Nothing, b::BitsSignal) = b
    @eval $op(a::AbstractSignal{S, N}, b::Integer) where {S<:Union{BitsSigType, Bool}, N} = 
        $op(a, ConstSig{S, N}(b))
    @eval $op(a::Integer, b::AbstractSignal{S, N}) where {S<:Union{BitsSigType, Bool}, N} = 
        $op(ConstSig{S, N}(a), b)
end
export nand, nor, xnor
import Base:~
~(p::AbstractSignal{S, N}) where {S<:Union{BitsSigType, Bool}, N} = BitwiseOperator{S, N}(false, :not, (p,))
function ~(op::BitwiseOperator{S, N}) where {S, N}
    return BitwiseOperator{S, N}(true, op.operator, op.from)
end

struct CmpOperator <: AbstractOperator{Bool, 1}
    invert::Bool
    operator::Symbol
    from::Tuple{Union{AbstractSignal{S, N}, Isig}, Union{AbstractSignal{S, N}, Isig}} where {S, N}
end
for (op, sym) in [(:eq, :(:(=))), (:neq, :(:(/=))),
                  (:<=, :(:<=)), (:<, :(:<)),
                  (:>=, :(:>=)), (:>, :(:>))]
    if !(op in [:eq, :neq])
        @eval import Base.$op        
    else
        @eval function $op(a::AbstractSignal{S, N}, b::AbstractSignal{S, N}) where {S, N}
            return CmpOperator(false, $sym, (a, b))
        end
    end
    @eval function $op(a::ArithSig{S, N}, b::ArithSig{S, N}) where {S <: Union{Signed, Unsigned}, N}
        return CmpOperator(false, $sym, (a, b))
    end
    @eval $op(a::ArithSig, ::Nothing) = a
    @eval $op(::Nothing, b::ArithSig) = b
    @eval $op(a::ArithSig{S,N}, b::T where T<:Integer) where {S, N} = 
        $op(a, ConstSig{S, N}(b))
    @eval $op(a::T where T<:Integer, b::ArithSig{S,N}) where {S, N} = 
        $op(ConstSig{S, N}(a), b)
end
export eq, neq

import Base:(!)
const invert_dict = Dict(:(=) => :(/=), :(/=) => :(=), 
                       :(<=) => :(>), :(>=) => :(<), 
                       :(>) => :(<=), :(<) => :(>=))
(!)(a::CmpOperator) = CmpOperator(false, invert_dict[a.operator], a.from)

struct ShiftOp{S, N} <: AbstractOperator{S, N}
    operator::Symbol
    from::AbstractSignal{S, N}
    ncount::Integer
end

# Shift operators
for (op, sym) in [(:<<, :(:shift_left)), (:>>, :(:shift_right)),
                  (:rol, :(:rotate_left)), (:ror, :(:rotate_right))]
    if !(op in [:rol, :ror])
        @eval import Base.$op                  
    end                      
    @eval function $op(a::AbstractSignal{S, N}, b::Integer) where {S, N}
        return ShiftOp{S, N}($sym, a, b)
    end
end
export rol, ror

function Base.adjoint(a::AbstractSignal{S, N} where {S<:Union{Signed,Unsigned}, N})
    return a
end

mutable struct Reg{S, N} <: AbstractSignal{S, N}
    clk::Wire
    rst::TNothing{Wire}
    from::TNothing{AbstractSignal{S, N}}
    blst::TNothing{AbstractVector{Reg}}
    init::ConstSig{S, N}
    rising_edge::Bool

    function Reg(clk::Wire, init::ConstSig{S, N}; 
        rst::TNothing{Wire} = nothing,
        rising_edge::Bool=true) where {S, N}
        return new{S, N}(clk, rst, nothing, nothing, init, rising_edge)
    end
    function Reg(reg::Reg{S, N};init::ConstSig{S, N} = ConstSig{S, N}()) where {S, N}
        reg2 = new{S, N}(reg.clk, reg.rst, reg, nothing, init, reg.rising_edge)
        if reg.blst !== nothing && reg.blst isa AbstractVector{Reg{S, N}}
            push!(reg.blst, reg2)
        else
            reg.blst = [reg, reg2]
            reg2.blst = reg.blst
        end
        return reg2 
    end
    function Reg{S, N}(clk::Wire;
        init::ConstSig{S, N} = ConstSig{S, N}(),
        rst::TNothing{Wire} = nothing,
        rising_edge::Bool=true) where {S<:Union{Bool, BitsSigType}, N}
        if length(init) !== N
            ArgumentError("length of initializer not equal to length of primitive!") |> throw
        end
        return new{S, N}(clk, rst, nothing, nothing, init, rising_edge)
    end
    function Reg{Integer, N}(clk::Wire;
        init::ConstSig{Integer, N} = ic{N}(),
        rst::TNothing{Wire} = nothing,
        rising_edge::Bool=true) where N
        return new{Integer, N}(clk, rst, nothing, nothing, init, rising_edge)
    end
    function Reg(clk::Wire, p::AbstractSignal{S, N}, pipeline::Int=1;
        rst::TNothing{Wire} = nothing,
        rising_edge::Bool=true) where {S, N}

        if pipeline > 1
            regs = SigVec(new{S, N}(clk, rst, nothing, nothing, ConstSig{S, N}()) for _ = 1:pipeline)
            for i = 1:pipeline-1
                regs[i].from = regs[i+1]
                regs[i].blst = regs
            end
            regs[end].from = p
            regs[end].blst = regs
            return regs
        else
            return new{S, N}(clk, rst, p, nothing, ConstSig{S, N}(), rising_edge)
        end
    end
end
function Reg{Integer}(clk::Wire;
    init::Integer=0,
    rst::TNothing{Wire} = nothing,
    rising_edge::Bool=true)
    return Reg{Integer, Int64(typemin(Int32)):Int64(typemax(Int32))}(clk, init=ic(init), rst=rst, rising_edge=rising_edge)
end
function Reg(clk::Wire, ps::AbstractSignal...; 
            rst::TNothing{Wire} = nothing,
            rising_edge::Bool=true)
    nps = length(ps)
    if nps > 1
        blst = [Reg(clk, p, 1, rst=rst) for p in ps]
        for r in blst
            r.blst = blst
        end
        return blst
    else
        return Reg(clk, ps..., 1; rst=rst, rising_edge=rising_edge)
    end
end
Ireg{P} = Reg{Integer, P}
Ureg{N} = Reg{Unsigned, N}
Sreg{N} = Reg{Signed, N}
Lreg{N} = Reg{Logical, N}
export Reg, Ireg, Ureg, Sreg, Lreg

function RegLoop(f::Function, clk::Wire, inits::ConstSig...; 
    rst::TNothing{Wire} = nothing,
    rising_edge::Bool=true,
    outreg::Bool=true)
    regs = Reg.(Ref(clk), inits; rst=rst, rising_edge=rising_edge)
    blst = [regs...]
    for reg in regs 
        reg.blst = blst
    end
    regs_nxt = f(regs...)
    regs .<| regs_nxt
    if outreg
        return regs
    else
        return regs_nxt
    end
end
export RegLoop

struct Mux{S, N} <: AbstractSignal{S, N}
    sel::AbstractSignal
    from::NTuple{L, T} where {L, T<:Pair{TNothing{ConstSig}, TNothing{AbstractSignal{S}}}}
    blst::TNothing{Vector{Mux}}

    function Mux(m::TNothing{Mux},
            sel::AbstractSignal{P, K}, 
            pairs::Pair{<:TNothing{ConstSig{P, K}}, <:AbstractSignal{S, N}}...) where {P, K, S, N}
        if any(p[1] === q[1] && p !== q for p in pairs, q in pairs)
            ArgumentError("duplicate case in mux.") |> throw
        end
        if m === nothing            
            blst = Vector{Mux}()
        elseif m.blst === nothing
            ArgumentError("mux can't be bundled.") |> throw
        else
            blst = m.blst
            if any(pairs == p.from for p in blst)
                ArgumentError("redundant mux.") |> throw
            end
        end

        mux = new{S, N}(sel, pairs, blst)
        push!(blst, mux)
        return mux
    end
    function Mux(m::TNothing{Mux},
        sel::AbstractSignal{P, K}, 
        pairs::Pair{<:TNothing{ConstSig{P, K}}, <:Isig}...) where {P, K}
        if any(p[1] === q[1] && p !== q for p in pairs, q in pairs)
            ArgumentError("duplicate case in mux.") |> throw
        end
        if m === nothing            
            blst = Vector{Mux}()
        elseif m.blst === nothing
            ArgumentError("mux can't be bundled.") |> throw
        else
            blst = m.blst
            if any(pairs == p.from for p in blst)
                ArgumentError("redundant mux.") |> throw
            end
        end

        l = minimum(range(p[2]).start for p in pairs)
        u = maximum(range(p[2]).stop for p in pairs)
        mux = new{Integer, l:u}(sel, pairs, blst)
        push!(blst, mux)
        return mux
    end
    function Mux(sel::AbstractSignal{P, K}, 
                pairs::Pair{<:TNothing{ConstSig{P, K}}, <:AbstractSignal{S, N}}...) where {P, K, S, N}
        k = findfirst(x-> x[1] === nothing, pairs)
        if k === nothing
            ArgumentError("others case (nothing) is missing.") |> throw
        elseif k < length(pairs)
            ArgumentError("others case (nothing) must be last element.") |> throw
        end
        new{S, N}(sel, pairs, nothing)
    end
    function Mux(sel::AbstractSignal{P, K}, 
        pairs::Pair{<:TNothing{ConstSig{P, K}}, <:Isig}...) where {P, K}
        k = findfirst(x-> x[1] === nothing, pairs)
        if k === nothing
            ArgumentError("others case (nothing) is missing.") |> throw
        elseif k < length(pairs)
            ArgumentError("others case (nothing) must be last argument.") |> throw
        end
        l = minimum(range(p[2]).start for p in pairs)
        u = maximum(range(p[2]).stop for p in pairs)
        new{Integer, l:u}(sel, pairs, nothing)
    end
end
export Mux

function Mux(m::TNothing{Mux},
    sel::AbstractSignal, 
    pairs::Pair{<:TNothing{String}, <:AbstractSignal{S, N}}...) where {S, N}
    Mux(m, sel, ((p[1] === nothing ? nothing : p[1]^sel) => p[2] for p in pairs)...)
end
function Mux(m::TNothing{Mux},
    sel::Isig, 
    pairs::Pair{<:TNothing{Integer}, <:AbstractSignal{S, N}}...) where {S, N}
    Mux(m, sel, ((p[1] === nothing ? nothing : p[1]^sel) => p[2] for p in pairs)...)
end
function Mux(m::TNothing{Mux},
    sel::AbstractSignal, 
    pairs::Pair{<:Any, <:AbstractVector{<:AbstractSignal}}...)
    L = length(pairs[1][2])

    a = Mux(m, sel, (p[1] => p[2][1] for p in pairs)...)
    SigVec((a, (Mux(a, sel, (p[1] => p[2][i] for p in pairs)...) for i = 2:L)... ))
end
function Mux(sel::AbstractSignal, 
    pairs::Pair{<:TNothing{String}, <:AbstractSignal{S, N}}...) where {S, N}
    Mux(sel, ((p[1] === nothing ? nothing : p[1]^sel) => p[2] for p in pairs)...)
end
function Mux(sel::Isig, 
    pairs::Pair{<:TNothing{Integer}, <:AbstractSignal{S, N}}...) where {S, N}
    Mux(sel, ((p[1] === nothing ? nothing : p[1]^sel) => p[2] for p in pairs)...)
end

Mux(m, sel, pairs) = Mux(m, sel, pairs...) 
Mux(sel, pairs) = Mux(sel, pairs...) 

struct Cond{S, N} <: AbstractSignal{S, N}
    from::NTuple{L, T} where {L, T<:Pair{TNothing{Boolean}, TNothing{AbstractSignal{S}}}}
    blst::TNothing{Vector{Cond}}

    function Cond(c::TNothing{Cond}, 
              pairs::Pair{<: TNothing{Boolean},<: TNothing{AbstractSignal{S, N}}}...) where {S, N}
        k = findfirst(x-> x[1] === nothing, pairs)
        if !isnothing(k) && k < length(pairs)
            error("else case (nothing) must be last case.")
        elseif c === nothing
            blst = Vector{Cond}()
        elseif c.blst === nothing
            error("conditional can't be bundled.")
        else
            blst = c.blst
            if any(pairs == p.from for p in blst)
                error("redundant condition.")
            end
            if length(pairs) != length(blst[end].from) || any(p[1] != q[1] for (p, q) in zip(pairs, blst[end].from)) 
                error("specified conditions not consistent with bundle.")
            end
        end
        cond = new{S, N}(pairs, blst)
        push!(blst, cond)
        return cond
    end
    function Cond(c::TNothing{Cond}, 
              pairs::Pair{<: TNothing{Boolean},<: TNothing{<:Isig}}...)
        k = findfirst(x-> x[1] === nothing, pairs)
        if !isnothing(k) && k < length(pairs)
            error("else case (nothing) must be last case.")
        elseif c === nothing
            blst = Vector{Cond}()
        elseif c.blst === nothing
            error("conditional can't be bundled.")
        else
            blst = c.blst
            if any(pairs == p.from for p in blst)
                error("redundant condition.")
            end
            if length(pairs) != length(blst[end].from) || any(p[1] != q[1] for (p, q) in zip(pairs, blst[end].from)) 
                error("specified conditions not consistent with bundle.")
            end
        end
        l = minimum(range(p[2]).start for p in pairs if p[2] !== nothing)
        u = maximum(range(p[2]).stop for p in pairs if p[2] !== nothing)
        cond = new{Integer, l:u}(pairs, blst)
        push!(blst, cond)
        return cond
    end

    function Cond(pairs::Pair{<: TNothing{Boolean},<: TNothing{AbstractSignal{S, N}}}...) where {S, N}
        k = findfirst(x-> x[1] === nothing, pairs)
        if k === nothing
            error("else case (nothing) is missing.")
        elseif k < length(pairs)
            error("else case (nothing) must be last case.")
        end
        return new{S, N}(pairs, nothing) 
    end
    function Cond(pairs::Pair{<: TNothing{Boolean},<: TNothing{<:Isig}}...)
        k = findfirst(x-> x[1] === nothing, pairs)
        if k === nothing
            error("else case (nothing) is missing.")
        elseif k < length(pairs)
            error("else case (nothing) must be last case.")
        end
        l = minimum(range(p[2]).start for p in pairs)
        u = maximum(range(p[2]).stop for p in pairs)
        return new{Integer, l:u}(pairs, nothing) 
    end
end
export Cond
function Cond(m::TNothing{Cond},
    pairs::Pair{<:Any, <:AbstractVector{<:AbstractSignal}}...)
    L = length(pairs[1][2])

    a = Cond(m, (p[1] => p[2][1] for p in pairs)...)
    SigVec((a, (Cond(a, (p[1] => p[2][i] for p in pairs)...) for i = 2:L)... ))
end
function Cond(pairs::Pair{<:Any, <:AbstractVector{<:AbstractSignal}}...)
    Cond(nothing, pairs...)
end
Cond(pairs)  = Cond(pairs...) 
Cond(c::TNothing{Cond}, pairs) = Cond(c, pairs...)

function <|(a::AbstractSignal{S, N}, b::AbstractSignal{S, N}) where {S, N} 
    a.from = b
    return a
end
export <|

struct PortMap{S, N} <: AbstractSignal{S, N}
    t::Type{<:AbstractComponent}
    from::NTuple{L, Pair{Symbol, <:Union{AbstractSignal, SigVec, SigMat}}} where L
    pname::String
    lib::String
    blst::AbstractVector{Union{PortMap, SigVec, SigMat}}

    function PortMap(t::Type{<:AbstractComponent}, 
        pairs::Pair{Symbol, <:Union{AbstractSignal, SigVec, SigMat}}...; lib = "work")
        
        names = fieldnames(t)
        types = fieldtypes(t)

        #Some manual error checking
        for (name, p) in pairs
            namedir = rsplit(name |> String, "_"; limit=2)
            if length(namedir) < 2
                ArgumentError("missing '_' delimiter between name and direction of port.") |> throw
            elseif !occursin(lowercase(namedir[2]), "io")
                ArgumentError("mnvalid direction :$(namedir[2]) specified for port \"$(namedir[1])\" in component. Must be 'i', 'o', or 'io' separated by '_'.") |> throw
            elseif !occursin("i", lowercase(namedir[2]))
                ArgumentError("attempting to map a source signal to an output port.") |> throw
            end

            k = findfirst(x -> name === x, names)
            if k === nothing
                ArgumentError("component $t does not have a port $name") |> throw
            end

            tp = typejoin( types[k], typeof(p) )

            if tp <: Union{AbstractSignal, SigVec, SigMat} && !hasproperty(tp, :parameters)
                ArgumentError("port $name has incompatible type with input.") |> throw
            end
        end

        #Get output indices (and some error checking)
        outidx = findall(names) do pname 
            namedir = rsplit(pname |> String, "_"; limit=2)
            if length(namedir) < 2
                error("Missing '_' delimiter between name and direction of port.")
            elseif !occursin(lowercase(namedir[2]), "io")
                error("Invalid direction :$(namedir[2]) specified for port \"$(namedir[1])\" in component. Must be 'i', 'o', or 'io' separated by '_'.")
            elseif occursin("o", lowercase(namedir[2]))
                return true
            end
            return false
        end

        blst = Vector{Union{PortMap, SigVec, SigMat}}(undef, length(outidx))
        outs = ntuple(length(outidx)) do i
            j = outidx[i]
            pname, _ = rsplit(names[j] |> String, "_"; limit=2)
            tp = types[j]

            if !hasproperty(tp, :parameters)
                error("port $pname has no parameters.") |> throw
            end

            if tp <: AbstractSignal
                S, N = tp.parameters
                out = new{S, N}(t, pairs, pname, lib, blst)
                blst[i] = out
                return out
            elseif tp <: SigVec
                S, N, L = tp.parameters
                out = SigVec(new{S, N}(t, pairs, "$(pname)_$l", lib, blst) for l=1:L)
                blst[i] = out
                return out
            elseif tp <: SigMat
                S, N, L, K = tp.parameters
                out = SigMat(new{S, N}(t, pairs, "$(pname)_$l$k", lib, blst) for l=1:L,k=1:K)
                blst[i] = out
                return out
            else
                error("port $pname has illegal type $(typeof(tp))")
            end
        end
        return NamedTuple{names[outidx]}(outs)
    end
end
export PortMap
