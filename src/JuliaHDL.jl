#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#
module JuliaHDL

abstract type AbstractInterface end
abstract type CompositeInterface <: AbstractInterface end
abstract type AbstractComponent{A} end
abstract type AbstractHDL end

export AbstractInterface, CompositeInterface, AbstractComponent

include("Primitives.jl")
include("VHDL_output_funcs.jl")
include("CodeGeneration.jl")
include("Utilities.jl")

end # module
