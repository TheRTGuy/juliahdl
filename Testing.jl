using JuliaHDL

struct PrimVec{S, N, L} <: AbstractVector{Primitive} 
    prims::NTuple{L, Primitive}
    function PrimVec(ps::Primitive{S, N}...) where {S, N}
        new{S, N, length(ps)}(ps)
    end
end
function PrimVec(ps)
    PrimVec(ps...)
end

Base.size(::PrimVec{S, N, L} where {S, N}) where L = (L,)
Base.IndexStyle(::Type{<:PrimVec}) = IndexLinear()
Base.getindex(vec::PrimVec, i::Int) = vec.prims[i]

a, b = Usig(5), Usig(5)

avec = PrimVec(a, b, a + b)
bvec = PrimVec(Usig(5) for _=1:3)
cvec = PrimVec(Lsig(5) for _=1:5)

