#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

using JuliaHDL

struct Counter{A, N} <: AbstractComponent{A}
    clk_i::Wire
    rst_i::Wire
    ctr_o::Usig{N}
end

function Counter(maxcnt = 10)
    clk, rst = Wire(), Wire()

    nbits = ndigits(maxcnt, base=2)
    ctr = Ureg{nbits}(clk, rst=rst)
    ctr <| Cond(ctr < maxcnt-1 => ctr + 1, nothing => 0uc{nbits})

    Counter{:behavior, nbits}(clk, rst, ctr)
end

ctr = Counter()
generate_file(ctr)
