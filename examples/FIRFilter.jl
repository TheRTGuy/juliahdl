#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

using JuliaHDL

struct FIRFilter{A} <: AbstractComponent{A}
    input_i::Usig{5}
    output_o::Usig{5}
    taps_i::UsigVec{5, 5}
    clk_i::Wire
end

function FIRFilter()
    taps = SigVec(Usig(5) for _ = 1:5)
    input = Usig(5)
    clk = Wire()
          
    states = Reg(clk, input, 5)     
    output = taps'*states

    FIRFilter{:behavior}(input, output, taps, clk)
end


fir_filter = FIRFilter()
generate_file(fir_filter, outputdir="work", outfile="fir_filter.vhd")