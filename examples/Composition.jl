using JuliaHDL

#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

# This example demonstrates how to compose a component from other components using
# the port map, i.e. structural definition of a component

struct Adder{A, N} <: AbstractComponent{A}
    in1_i::Usig{N}
    in2_i::Usig{N}
    output_o::Usig{N}
end

function Adder(n)
    in1, in2 = Usig(n), Usig(n)
    return Adder{:rtl, n}(in1, in2, in1 + in2)
end

struct Multiplier{A, N} <: AbstractComponent{A}
    in1_i::Usig{N}
    in2_i::Usig{N}
    output_o::Usig{N}
end
function Multiplier(n)
    in1, in2 = Usig(n), Usig(n)
    return Multiplier{:rtl, n}(in1, in2, in1 * in2)
end

struct MulAdd{A, N} <: AbstractComponent{A}
    in1_i::Usig{N}
    in2_i::Usig{N}
    in3_i::Usig{N}
    output_o::Usig{N}
end

function MulAdd(n)
    in1, in2, in3 = (Usig(n) for _=1:3)
    (out_add,) = PortMap(Adder{:rtl, n}, :in1_i => in1, :in2_i => in2)
    (out_mul,) = PortMap(Multiplier{:rtl, n}, :in1_i => out_add, :in2_i => in3)

    MulAdd{:rtl, n}(in1, in2, in3, out_mul)
end

add = Adder(5)
mul = Multiplier(5)
madd = MulAdd(5)

generate_file.((add, mul, madd), outputdir="./work")
