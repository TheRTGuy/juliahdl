using JuliaHDL

struct MatMul{N, A} <: AbstractComponent{A} 
    A_i::UsigMat{N, 2, 3}
    x_i::UsigVec{N, 3}
    y_o::UsigVec{N, 2}
end

function MatMul(n)
    A = SigMat(Usig(n) for _=1:2,_=1:3)
    x = SigVec(Usig(n) for _=1:3)
    y = A*x
    MatMul{n,:arch}(A, x, y)
end

matmul = MatMul(3)
generate_file(matmul, outputdir="work", outfile="matmul.vhd")