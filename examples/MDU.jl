using JuliaHDL

struct MDU{A} <: AbstractComponent{A}
    mdu_l_i::Usig{32}
    mdu_r_i::Usig{32}
    mdu_hi_o::Usig{32}
    mdu_lo_o::Usig{32}
    mode_i::Wire
    rdy_o::Wire
    start_i::Wire
    clk_i::Wire
    rst_i::Wire
end


function divu32(a::UInt32, b::UInt32)
    if b == zero(b)
        return zero(a), zero(b)
    end
    c = zero(a)
    for i = 1:32
        #shift
        c = (c << 1) | (a >> 31)
        a = a << 1

        if c >= b
            c = c - b
            a = a | one(a)
        end
    end
    return a, c
end

function mulu32_booth(a::Int32, b::Int32)
    if b == zero(b)
        return zero(a), zero(b)
    end
    hi, lo, lsb = zero(a), b, false
    for i = 1:32
        if (lo & one(lo)) == one(lo) && lsb == zero(lsb)
            hi = hi - a
        elseif (lo & one(lo)) == zero(lo) && lsb == one(lsb)
            hi = hi + a
        end
        lsb = Bool(lo & one(lo))
        lo = (lo >>> 1) | (hi << 31)
        hi = hi >> 1
    end
    return hi, lo
end

function MDU()
    clk, rst = Wire(), Wire()
    start = Wire()
    mode = Wire()

    mdu_l, mdu_r = Usig(32), Usig(32)

    loreg, hireg, lsb, busy, ctr, mode_reg = 
        RegLoop(clk, uc{32}(0), uc{32}(0), lc"0", lc"0", 0ic{0:32}, lc"0", rst=rst) do loreg, hireg, lsb, busy, ctr, mode_reg
        
        nmdu_r = ~mdu_r + 1
        nmdu_l = ~mdu_l + 1
             
        hireg_shift = Usig(hireg[end-1 => 0] ^ loreg[end])
        sel = Cond(eq(mode_reg, lc"1") => (lc"0" ^ loreg[0] ^ lsb),
                        nothing => Wire(hireg_shift >= mdu_r) ^ lc"00")
        add_r = Mux(sel, lc"010" => nmdu_l, lc"001" => mdu_l, lc"100" => nmdu_r, nothing => uc("0"^32))
        add_l = Cond(eq(mode_reg, lc"1") => hireg, nothing => hireg_shift)

        adder = add_l + add_r

        @select begin
            @when busy == lc"1" begin
                @select begin 
                    @when eq(mode_reg, lc"1") begin #multiply mode
                        hireg_nxt = Usig(adder[end-1] ^ adder[end => 1])
                        loreg_nxt = Usig(hireg[0] ^ Usig(loreg[end => 1]))
                        lsb_nxt = loreg[0]
                    end
                    @others begin #divide mode
                        hireg_nxt = adder
                        loreg_nxt = Usig(loreg[end-1 => 0] ^ Wire(hireg_shift >= mdu_r))
                        lsb_nxt = lc"-" # don't care
                    end
                end
                
                @select begin
                    @when ctr < 31 begin 
                        ctr_nxt = ctr + 1
                        busy_nxt = lc"1"
                    end
                    @others begin 
                        ctr_nxt = 0ic{0:32}
                        busy_nxt = lc"0"
                    end
                end
            end
            @when start == lc"1" begin
                mode_reg_nxt = lc"1"
                hireg_nxt = uc("0"^32)
                loreg_nxt = mdu_l
                lsb_nxt = lc"0"
                busy_nxt = lc"1"
                ctr_nxt = 0ic{0:32}
            end
        end
        loreg_nxt, hireg_nxt, lsb_nxt, busy_nxt, ctr_nxt, mode_reg_nxt
    end

    rdy = Wire(eq(ctr, 31) & eq(busy, lc"1"))
    return MDU{:arch}(mdu_l, mdu_r, hireg, loreg, mode, rdy, start, clk, rst)
end

@info divu32(UInt32(30),UInt32(3))

hi, lo = mulu32_booth(typemax(Int32), typemax(Int32))
@info reinterpret(Int64, [lo, hi])

mdu = MDU()
generate_file(mdu, outputdir="work", outfile="mdu.vhd")