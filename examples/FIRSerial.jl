#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#


#=
    This example demonstrates a FIR with internal taps, loaded by a serial interface
=#

using JuliaHDL
# using JuliaHDL.Interfaces

struct FIRSerial{N, A} <: AbstractComponent{A}
    clk_i::Wire
    resetn_i::Wire

    tap_i::Usig{N}
    write_i::Wire
    busy_o::Wire

    input_i::Usig{N}
    output_o::Usig{N}
end

function FIRSerial(dwidth::Integer, fir_len::Integer) 
    input = Usig(dwidth)
    tap_in = Usig(dwidth)
    clk, rst = Wire(), Wire()
    write = Wire()

    taps = SigVec(Ureg{dwidth}(clk, rst=~rst) for _=1:fir_len)
    regs = Reg(clk, input, fir_len)

    states = (working = ic{0:1}(0), writing = ic{0:1}(1))

    ctr = Ireg{0:fir_len}(clk, rst=~rst)
    state = Ireg{0:1}(clk, rst=~rst)
    @select state begin
        @when states.working begin
            busy = lc"0"
            output = regs'taps
            taps_next = taps
            @select begin
                @when write == lc"1" begin 
                    state_nxt = states.writing
                    ctr_nxt = 0ic{0:fir_len}
                end
                @others state_nxt = state
            end
        end
        @when states.writing begin
            output = uc("0"^dwidth)
            taps_next = vcat(tap_in, taps[1:end-1])
            busy = lc"1"
            @select begin
                @when ctr < fir_len begin
                    ctr_nxt = ctr + 1
                    state_nxt = states.writing
                end
                @others begin
                    ctr_nxt = 0ic{0:fir_len}
                    state_nxt = states.working
                end
            end
        end
    end
    state <| state_nxt
    ctr <| ctr_nxt
    taps .<| taps_next

    FIRSerial{dwidth, :arch}(clk, rst, tap_in, write, busy, input, output)
end

fir_ser = FIRSerial(32, 5)

generate_file(fir_ser, outputdir="work", outfile="fir_ser.vhd")