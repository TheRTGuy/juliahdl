#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

using JuliaHDL

#=
Simple (and dumb) example of slicing and concatenation in JuliaHDL
=#

struct Slices{A} <: AbstractComponent{A}
    inp_i::Usig{10}
    lout_o::Lsig{5}
    uout_o::Lsig{5}
end

function Slices()
    in = Usig(10)

    s1 = Lsig(in[2 => 0]) ^ in[3:4]
    s2 = Lsig(in[8 => 5]) ^ in[9]


    return Slices{:behavior}(in, s1, s2)
end

slices = Slices()
generate(slices)