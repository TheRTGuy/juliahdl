#=
Copyright (c) 2021 Viktorio S. el Hakim

This Source Code Form is subject to the terms of the Mozilla Public License, 
v. 2.0. If a copy of the MPL was not distributed with this file, 
You can obtain one at https://mozilla.org/MPL/2.0/.
=#

using JuliaHDL

struct AdderChain{A} <: AbstractComponent{A}
    in_i::UsigVec{5, 5} #5 primitives of length 5
    out_o::Usig{5}
end

function AdderChain()
    inputs = SigVec(Usig(5) for _=1:5)
    output = sum(inputs)

    AdderChain{:behavior}(inputs, output)
end

adder_chain = AdderChain()
generate(adder_chain)