using JuliaHDL


struct MultAdd{A} <: AbstractComponent{A}
    in1_i::Usig{5}
    in2_i::Usig{5}
    func_i::Lsig{2}
    output1_o::Usig{5}
    output2_o::Usig{5}
end

function MultAdd()
    in1, in2 = Usig(5), Usig(5)
    func = Lsig(2)

    Func = (ADD = "00", MUL = "01")
    @select func begin
        @when Func.MUL res1 = in1 * in2
        @others res1 = in1 + in2
    end
    @select func begin
        @when Func.ADD res2 = in1 + in2
        @others res2 = in1 * in2
    end

    MultAdd{:behavior}(in1, in2, func, res1, res2)
end

struct MinMax{A} <: AbstractComponent{A} 
    in1_i::Usig{5}
    in2_i::Usig{5}
    min_o::Usig{5}
    max_o::Usig{5}
end

function MinMax()
    in1, in2 = Usig(5), Usig(5)
    @select begin
        @when in1 > in2 begin
            max = in1
            min = in2
        end
        @others begin
            max = in2
            min = in1
        end
    end
    MinMax{:behavior}(in1, in2, min, max)
end

madd = MultAdd()
minmax = MinMax()

generate_file(madd, outputdir="work", outfile="madd.vhd")
generate_file(minmax, outputdir="work", outfile="minmax.vhd")
